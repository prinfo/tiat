#!/bin/bash

_tiat() {
    local options="-c --config -t --test"
    local cur=${COMP_WORDS[COMP_CWORD]}
    local prev=${COMP_WORDS[COMP_CWORD-1]}

    if [[ " ${prev} " == " -c " || " ${prev} " == " --config " ]]; then
	    compopt -o default
	    return 0
    fi

    COMPREPLY=( $(compgen -W "${options}" -- $cur) )
}

complete -F _tiat tiat
