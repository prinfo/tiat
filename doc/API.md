Authorization
-------------

An API call requires a string to be added to tiat.conf and an HTTP Authorization header in the request.
```
"tokens": ["0123456789"]
```

```
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/clients
```

DHCP
====

Services
-------
Return all services
```
GET /dhcp/services
```

Return a single service
```
GET /dhcp/services/{id}
```

Add a service

Requires JSON data with the following keys: 
* service (Required, string)
* relayagent (Required, ip)
* clientlimit (Optional, Default = 0, int)
```
POST /dhcp/services
```

Delete a service
```
DELETE /dhcp/services/{id}
```

Update an existing service

Requires JSON data with the following keys:
* id (Required, int)
* service (Required, string)
* relayagent (Required, ip)
* clientlimit (Optional, Default = 0, int)
```
PUT /dhcp/services
```

Subnets
-------
Return all services
```
GET /dhcp/subnets
```

Return a single subnet
```
GET /dhcp/subnets/{id}
```

Add a subnet

Requires JSON data with the following keys: 
* subnet (Required, string)
* allowunknown (Optional, Default = false, bool)
* isstatic (Optional, Default = false, bool)
* service (Required, int)
```
POST /dhcp/subnets
```

Delete a subnet
```
DELETE /dhcp/subnets/{id}
```

Update an existing subnet

Requires JSON data with the following keys:
* id (Required, int)
* subnet (Required, string)
* allowunknown (Optional, Default = false, bool)
* isstatic (Optional, Default = false, bool)
* service (Required, int)
```
PUT /dhcp/subnets
```

Pools
-------
Return all pools
```
GET /dhcp/pools
```

Return a single pool
```
GET /dhcp/pools/{id}
```

Add a pool

Requires JSON data with the following keys:
* poolstart (Required, ip)
* poolend (Required, ip)
* subnet (Required, int)
```
POST /dhcp/pools
```

Delete a pool
```
DELETE /dhcp/pools/{id}
```

Update an existing pool

Requires JSON data with the following keys:
* id (Required, int)
* poolstart (Required, ip)
* poolend (Required, ip)
```
PUT /dhcp/pools
```

Pool Options
-------

Return a pool's options
```
GET /dhcp/pooloptions/{pool}
```

Add a pool's option

Requires JSON data with the following keys: 
* option (Required, int)
* value (Required, string)
* subnet (Required, int)
```
POST /dhcp/pooloptions
```

Delete a pool's option
```
DELETE /dhcp/pooloptions/{id}
```

Update a pool's option

Requires JSON data with the following keys:
* id (Required. int)
* value (Required, string)
```
PUT /dhcp/pooloptions
```

Clients
-------
Return all clients
```
GET /dhcp/clients
```

Return a specific client based on the id
```
GET /dhcp/clients/{id}
```

Return all clients based on the clientid
```
GET /dhcp/clients/clientid/{clientid}
```

Return a specific client based on the value
```
GET /dhcp/clients/value/{value}
```

Add a client

Requires JSON data with the following keys: 
* clientid (Optional, string)
* value (Required, string)
* ipcount (Optional, Default = 1) 
* service (Required, int)
* subnet (Optional, int)
```
POST /dhcp/clients
```

Delete a client based on it's id
```
DELETE /dhcp/clients/{id}
```

Update a client

Requires JSON data with the following keys: 
* id (Required, int)
* clientid (Required, string)
* service (Required, int)
* value (Required, string)
* ipcount (Required, int)
```
PUT /dhcp/clients
```

Client IPs
-------

Return all configured ips
```
GET /dhcp/clientips
```

Return a specific client's IP
```
GET /dhcp/clientips/{client}
```

Add an IP

Requires JSON data with the following keys: 
* ipaddress (Required, ip)
* client (Required, int)
```
POST /dhcp/clientips
```

Delete an IP
```
DELETE /dhcp/clientips/{id}
```

Update an IP

Requires JSON data with the following keys:
* id (Required, int)
* ipaddress (Required, ip)
```
PUT /dhcp/clientips
```

Client Options
-------

Return a client's options
```
GET /dhcp/clientoptions/{client}
```

Add a client's option

Requires JSON data with the following keys: 
* option (Required, int)
* value (Required, string)
* client (Required, int)
```
POST /dhcp/clientoptions
```

Delete a client's option
```
DELETE /dhcp/clientoptions/{id}
```

Update a client's option

Requires JSON data with the following keys:
* id (Required. int)
* value (Required, string)
```
PUT /dhcp/clientoptions
```

Leases
-------

Return all active leases
```
GET /dhcp/leases
```

Return an active lease based on mac address. Mac format needs to be xx:xx:xx:xx:xx:xx
```
GET /dhcp/leases/{mac}
```

Return an active lease based on option82
```
GET /dhcp/leases/{option82}
```

Delete an active lease
```
DELETE /dhcp/leases/{mac}?ip=<ip>
```

Vendor Options
-------
Return all options
```
GET /dhcp/vendoroptions
```

Return a single option
```
GET /dhcp/vendoroptions/{id}
```

Add an option

Requires JSON data with the following keys:
* name (Required, string)
* suboption (Required, int)
* value (Required, string)
* type (Required, int) See Vendor Types
```
POST /dhcp/vendoroptions
```

Delete an option
```
DELETE /dhcp/vendoroptions/{id}
```

Update an existing option

Requires JSON data with the following keys:
* id (Required, int)
* name (Required, string)
* value (Required, string)
* type (Required, int) See Vendor Types
```
PUT /dhcp/vendoroptions
```

Vendor Types
-------
```
 ID |    Type    
----+------------
  1 | String
  2 | Uint8
  3 | Uint16
  4 | Uint32
  5 | IP-Address
  6 | Bool
  7 | Hex
```

BECS
====

ASRs
----
Return all ASRs
```
GET /becs/asrs
```

Return a single asr by name
```
GET /becs/asrs/{name}
```

Return status of an ASR
```
GET /becs/asrstatus/{cell}/{oid}
```

Interfaces
----------
Return interfaces below given oid
```
GET /becs/interfaces/{asroid}
```

Return status of an interface
```
GET /becs/interfacestatus/{oid}
```

Services
--------
Return BECS services starting from given oid

servicename and namespace query can be used to filter services
```
GET /becs/services/{parentoid}
```

Add a service

Requires JSON data with the following keys:
* interfaceoid (Required, int)
* service (Required, string)
* namespace (Optional, string)
* attributes (Optional, array with string keys "name" and "currentValue" ) 
```
POST /becs/services
```

Delete a service
```
DELETE /becs/services/{saoid}
```

Disable a service
```
POST /becs/services/disable/{saoid}
```

Enable a service
```
POST /becs/services/enable/{saoid}
```

Modify a service

Requires JSON data with the following keys:
* saoid (Required, int)
* attributes (Optional, array with string keys "name" and "currentValue" ) 
```
PUT /becs/services
```

Clients
-------
Return clients from an interface
```
GET /becs/clients/{interfaceoid}
```

Delete a client
```
DELETE /becs/clients/{asroid}?context=
```

You can optionally add a reason to the query. Example: "reason=deactivated"