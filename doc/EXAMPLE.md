DHCP
====

Services
--------

First you need to add relay-agents related to your services.

Let's say my internet service clients are coming from relay agent 100.64.0.1...

```
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/services -d '{"service": "Internet", "relayagent": "100.64.0.1"}'
```


Subnets
-------

You need to add a subnet for your service.

Here you can choose if the subnet is going to allow unknown clients or not, or if the subnet will be used for IP reservations.

Let's say I want the subnet 100.64.0.0/24 to only hand out IPs to clients that I allow...

```
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/subnets -d '{"subnet": "100.64.0.0/24", "service": 1}'
```

allowunknown and isstatic is false by default and "1" is the id of the internet service.

You can have multiple subnets related to a service.

You can for example have one subnet that doesn't allow unknown clients and one that does, or more of the same type if you run out of IPs for a scope.

Pools
-----

To specify the range of available IPs for a subnet...

```
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/pools -d '{"poolstart": "100.64.0.2", "poolend": "100.64.0.254", "subnet", 1}'
```

"1" is the id of the 100.64.0.0/24 subnet. 

Pool options
------------

Here you provide options that you want available for a subnet.

The subnet 100.64.0.0/24 will have the options 1, 3, 6 and 51.

1 = subnetmask. 

3 = gateway. 

6 = name server. 

51 = lease time.

```
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/pooloptions -d '{"option": 1, "value": "255.255.255.0", "subnet": 1}'
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/pooloptions -d '{"option": 3, "value": "100.64.0.1", "subnet": 1}'
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/pooloptions -d '{"option": 6, "value": "8.8.8.8", "subnet": 1}'
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/pooloptions -d '{"option": 51, "value": "1200", "subnet": 1}'
```

If an option allows multiple values you can separate them with a comma. If you have 2 name servers for example ('8.8.8.8,8.8.4.4').

Vendor specific option (Option 43)
----------------------

If you want to provide vendor specific options you can define the option in dhcp4_vendoroptions.

```
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/vendoroptions -d '{"name": "HES-3109.configuration-file", "suboption": 7, "value": "test.conf", "type": 1}'
```

You can find the available types in API.md. 1 = String.

You can then add the option to dhcp4_pooloptions or dhcp4_clientoptions with the vendor option name as value.

```
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/pooloptions -d '{"option": 43, "value": "HES-3109.configuration-file", "subnet": 1}'
```

Known clients
-------------

If you've defined a subnet that doesn't allow unknown clients we need to add them to dhcp4_knownclients.

```
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/clients -d '{"clientid": "prinfo", "value": "ethernet1/1_switch-1a", "service": 1}'
```

Value should be either the client's option82 value or mac-address. Clientid is optional and ipcount is 1 by default. You can also specify a subnet for a client.

If you're adding both circuitid and remoteid you need to separate them with an underscore. You can add circuitid alone but not remoteid.

IP reservation
--------------

If you've defined a subnet as "isstatic" you need to add them to dhcp4_staticips.

```
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/clientips -d '{"ipaddress": "100.64.0.10", "client": 1}'
```

Client specific options
-----------------------

You can add or override an option for a specific client.

Let's say you want one CPE to have a different bootfile than the others.

```
curl -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/clientoptions -d '{"option": 67, "value": "cpe1.bin", "client": 1}'
```

Reload Configuration
==========================

If you have added or changed a client, clientoptions or clientips there's no need to reload the configuration.
Otherwise you can reload it with...

```
curl -X POST -H "Authorization: 0123456789" http://127.0.0.1:8033/dhcp/reload
```

The reload call will abort if it finds any configuration errors.
