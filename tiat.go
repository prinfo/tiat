package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"gitlab.com/prinfo/tiat/becs"
	"gitlab.com/prinfo/tiat/dhcp"
	"gitlab.com/prinfo/tiat/metric"
)

//Settings holds the configuration from tiat.conf
type Settings struct {
	API struct {
		Address string   `json:"listen_address"`
		Tokens  []string `json:"tokens"`
	} `json:"api"`

	BECS struct {
		Enable   bool   `json:"enable"`
		Host     string `json:"host"`
		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"becs"`

	DHCP struct {
		Enable     bool     `json:"enable"`
		Addresses  []net.IP `json:"listen_addresses"`
		DBHost     string   `json:"database_host"`
		DBName     string   `json:"database_name"`
		DBUser     string   `json:"database_user"`
		DBPassword string   `json:"database_password"`
	} `json:"dhcp"`

	Metrics struct {
		Enable bool   `json:"enable"`
		URL    string `json:"influx_host"`
		DBName string `json:"influx_database"`
	} `json:"metrics"`
}

const usage string = `
Usage: tiat [OPTIONS]

Options:
  -c, --config	Path to configuration file (default "/etc/tiat/tiat.conf")
  -t, --test	Test DHCP configuration and connection to database
`

func main() {
	var configFile string
	var configTest bool
	flag.StringVar(&configFile, "c", "/etc/tiat/tiat.conf", "Path to configuration file")
	flag.StringVar(&configFile, "config", "/etc/tiat/tiat.conf", "Path to configuration file")
	flag.BoolVar(&configTest, "t", false, "Test DHCP configuration and connection to database")
	flag.BoolVar(&configTest, "test", false, "Test DHCP configuration and connection to database")
	flag.Usage = func() { fmt.Println(usage) }
	flag.Parse()

	//Read config file
	setupContents, err := os.ReadFile(configFile)
	if err != nil {
		log.Fatalln(err)
	}

	//Parse config file
	var settings Settings
	if err := json.Unmarshal(setupContents, &settings); err != nil {
		log.Fatalln("config file:", err)
	}

	router := mux.NewRouter()

	//InfluxDB client configuration
	if settings.Metrics.Enable {
		fmt.Println("Metrics Enabled")
		metric.Open(settings.Metrics.URL, settings.Metrics.DBName)
		defer metric.Close()
	}

	//BECS configuration
	if settings.BECS.Enable {
		fmt.Println("BECS Enabled")

		becs.Host = settings.BECS.Host
		becs.Username = settings.BECS.Username
		becs.Password = settings.BECS.Password
		becs.APITokens = settings.API.Tokens

		router.HandleFunc("/becs/asrs", becs.GetASRs).Methods("GET")
		router.HandleFunc("/becs/asrs/{name}", becs.GetASR).Methods("GET")
		router.HandleFunc("/becs/asrstatus/{cell}/{oid}", becs.GetASRStatus).Methods("GET")

		router.HandleFunc("/becs/interfaces/{asroid}", becs.GetASRInterfaces).Methods("GET")
		router.HandleFunc("/becs/interfacestatus/{oid}", becs.GetInterfaceStatus).Methods("GET")

		router.HandleFunc("/becs/services/{parentoid}", becs.GetServices).Methods("GET")
		router.HandleFunc("/becs/services", becs.AddService).Methods("POST")
		router.HandleFunc("/becs/services/{saoid}", becs.DelService).Methods("DELETE")
		router.HandleFunc("/becs/services/disable/{saoid}", becs.DisableService).Methods("POST")
		router.HandleFunc("/becs/services/enable/{saoid}", becs.EnableService).Methods("POST")
		router.HandleFunc("/becs/services", becs.ModifyService).Methods("PUT")

		router.HandleFunc("/becs/clients/{interfaceoid}", becs.GetClients).Methods("GET")
		router.HandleFunc("/becs/clients/{asroid}", becs.DelClient).Methods("DELETE")
	}

	//DHCP server configuration
	if settings.DHCP.Enable {

		//Format postgres connection info
		psqlInfo := fmt.Sprintf("host=%s port=%d dbname=%s "+
			"user=%s password=%s sslmode=disable connect_timeout=5",
			settings.DHCP.DBHost, 5432, settings.DHCP.DBName,
			settings.DHCP.DBUser, settings.DHCP.DBPassword)

		//Prepare postgres connection.
		psql, err := sql.Open("postgres", psqlInfo)
		if err != nil {
			log.Fatalln(err)
		}
		defer psql.Close()

		//If -t flag is set we test the DHCP and exit
		if configTest {
			if err := dhcp.Test(psql); err != nil {
				log.Fatalln(err)
			}

			fmt.Println("Configuration OK")
			os.Exit(0)
		}

		fmt.Println("DHCP Enabled")

		dhcp := dhcp.Server{
			APITokens: settings.API.Tokens,
			DBHost:    settings.DHCP.DBHost,
		}

		go func() {
			if err := dhcp.Start(settings.DHCP.Addresses, psql); err != nil {
				log.Fatalln(err)
			}
		}()

		router.HandleFunc("/dhcp/reload", dhcp.Reload).Methods("POST")

		router.HandleFunc("/dhcp/services", dhcp.GetServices).Methods("GET")
		router.HandleFunc("/dhcp/services/{id}", dhcp.GetService).Methods("GET")
		router.HandleFunc("/dhcp/services", dhcp.AddService).Methods("POST")
		router.HandleFunc("/dhcp/services/{id}", dhcp.DelService).Methods("DELETE")
		router.HandleFunc("/dhcp/services", dhcp.UpdateService).Methods("PUT")

		router.HandleFunc("/dhcp/subnets", dhcp.GetSubnets).Methods("GET")
		router.HandleFunc("/dhcp/subnets/{id}", dhcp.GetSubnet).Methods("GET")
		router.HandleFunc("/dhcp/subnets", dhcp.AddSubnet).Methods("POST")
		router.HandleFunc("/dhcp/subnets/{id}", dhcp.DelSubnet).Methods("DELETE")
		router.HandleFunc("/dhcp/subnets", dhcp.UpdateSubnet).Methods("PUT")

		router.HandleFunc("/dhcp/pools", dhcp.GetPools).Methods("GET")
		router.HandleFunc("/dhcp/pools/{id}", dhcp.GetPool).Methods("GET")
		router.HandleFunc("/dhcp/pools", dhcp.AddPool).Methods("POST")
		router.HandleFunc("/dhcp/pools/{id}", dhcp.DelPool).Methods("DELETE")
		router.HandleFunc("/dhcp/pools", dhcp.UpdatePool).Methods("PUT")

		router.HandleFunc("/dhcp/pooloptions", dhcp.GetPoolsOptions).Methods("GET")
		router.HandleFunc("/dhcp/pooloptions/{pool}", dhcp.GetPoolOptions).Methods("GET")
		router.HandleFunc("/dhcp/pooloptions", dhcp.AddPoolOption).Methods("POST")
		router.HandleFunc("/dhcp/pooloptions/{id}", dhcp.DelPoolOption).Methods("DELETE")
		router.HandleFunc("/dhcp/pooloptions", dhcp.UpdatePoolOption).Methods("PUT")

		router.HandleFunc("/dhcp/leases", dhcp.GetLeases).Methods("GET")
		router.HandleFunc("/dhcp/leases/{mac:(?:[0-9a-f]{2}:){5}[0-9a-f]{2}}", dhcp.GetLeaseByMac).Methods("GET")
		router.HandleFunc("/dhcp/leases/{opt82}", dhcp.GetLeaseByOpt82).Methods("GET")
		router.HandleFunc("/dhcp/leases/{mac}", dhcp.DelLease).Methods("DELETE")

		router.HandleFunc("/dhcp/clients", dhcp.GetClients).Methods("GET")
		router.HandleFunc("/dhcp/clients/{id}", dhcp.GetClient).Methods("GET")
		router.HandleFunc("/dhcp/clients/value/{value}", dhcp.GetClientByValue).Methods("GET")
		router.HandleFunc("/dhcp/clients/clientid/{clientid}", dhcp.GetClientByClientID).Methods("GET")
		router.HandleFunc("/dhcp/clients", dhcp.AddClient).Methods("POST")
		router.HandleFunc("/dhcp/clients/{id}", dhcp.DelClient).Methods("DELETE")
		router.HandleFunc("/dhcp/clients", dhcp.UpdateClient).Methods("PUT")

		router.HandleFunc("/dhcp/clientips", dhcp.GetIPs).Methods("GET")
		router.HandleFunc("/dhcp/clientips/{clientid}", dhcp.GetIP).Methods("GET")
		router.HandleFunc("/dhcp/clientips", dhcp.AddIP).Methods("POST")
		router.HandleFunc("/dhcp/clientips/{id}", dhcp.DelIP).Methods("DELETE")
		router.HandleFunc("/dhcp/clientips", dhcp.UpdateIP).Methods("PUT")

		router.HandleFunc("/dhcp/clientoptions", dhcp.GetClientsOptions).Methods("GET")
		router.HandleFunc("/dhcp/clientoptions/{clientid}", dhcp.GetClientOptions).Methods("GET")
		router.HandleFunc("/dhcp/clientoptions", dhcp.AddClientOption).Methods("POST")
		router.HandleFunc("/dhcp/clientoptions/{id}", dhcp.DelClientOption).Methods("DELETE")
		router.HandleFunc("/dhcp/clientoptions", dhcp.UpdateClientOption).Methods("PUT")

		router.HandleFunc("/dhcp/vendoroptions", dhcp.GetVendorOptions).Methods("GET")
		router.HandleFunc("/dhcp/vendoroptions/{id}", dhcp.GetVendorOption).Methods("GET")
		router.HandleFunc("/dhcp/vendoroptions", dhcp.AddVendorOption).Methods("POST")
		router.HandleFunc("/dhcp/vendoroptions/{id}", dhcp.DelVendorOption).Methods("DELETE")
		router.HandleFunc("/dhcp/vendoroptions", dhcp.UpdateVendorOption).Methods("PUT")
	}

	httpSrv := http.Server{
		Handler:      router,
		Addr:         settings.API.Address,
		WriteTimeout: 10 * time.Second,
		ReadTimeout:  10 * time.Second,
		IdleTimeout:  0,
	}
	log.Fatalln(httpSrv.ListenAndServe())
}
