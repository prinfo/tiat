package dhcp

import (
	"bytes"
	"net"
	"strconv"
)

//DHCP Message types
const (
	_              = iota
	Discover uint8 = iota
	Offer
	Request
	Decline
	Ack
	Nak
	Release
	Inform
)

type dhcpHeader struct {
	OpCode        uint8
	HardwareType  uint8
	HardwareLen   uint8
	Hops          uint8
	TransactionID [4]byte
	Seconds       [2]byte
	Flags         [2]byte
	CiAddr        [4]byte
	YiAddr        [4]byte
	SiAddr        [4]byte
	GiAddr        [4]byte
	ChAddr        [6]byte
	ChPadding     [10]byte
	ServerName    [64]byte
	BootFile      [128]byte
	Cookie        [4]byte
}

type dhcpOptions struct {
	data []byte
}

//addOptions appends the option, length and value to data byte array
func (d *dhcpOptions) addOption(option uint8, value []byte) {
	d.data = append(d.data, option)

	switch option {
	case 1, 3, 4, 5, 6, 7, 28, 42, 69, 81:

		//If value contains "," split the value and multiply the size
		if bytes.Contains(value, []byte(",")) {
			splitValue := bytes.Split(value, []byte(","))
			d.data = append(d.data, uint8(4*len(splitValue)))
			for _, v := range splitValue {
				d.data = append(d.data, net.ParseIP(string(v)).To4()...)
			}
		} else {
			d.data = append(d.data, 4)
			d.data = append(d.data, net.ParseIP(string(value)).To4()...)
		}
	case 2, 51:

		/*The int value comes as a []byte to we need to convert it
		to the right size*/
		bytes := d.intStringtoArray(string(value), 32)
		d.data = append(d.data, 4)
		d.data = append(d.data, bytes...)
	case 15, 66, 67:
		d.data = append(d.data, uint8(len(value)))
		d.data = append(d.data, value...)
	}
}

//optionValue returns the given option's value
func (d *dhcpOptions) optionValue(option uint8) []byte {

	//Option size is always at option +1, and the data is at option +2
	for i := 0; i < len(d.data); {

		//End of options, the rest is just padding
		if d.data[i] == 255 {
			return nil
		}

		size := int(d.data[i+1])
		if d.data[i] == option {
			return d.data[i+2 : i+2+size]
		}

		i += int(size) + 2
	}
	return nil
}

/*option82 returns the circuitID and remoteID concatenated with "_"

Returns only circuitID if remoteID is empty*/
func (d *dhcpOptions) option82() string {
	value := d.optionValue(82)
	if value == nil {
		return ""
	}

	circuitSize := value[1]
	circuitID := value[2 : 2+circuitSize]

	if byte(len(value)) > circuitSize+2 {
		remoteSize := value[circuitSize+3]
		remoteID := value[circuitSize+4 : circuitSize+4+remoteSize]
		return string(circuitID) + "_" + string(remoteID)
	}

	return string(circuitID)
}

func (d *dhcpOptions) intStringtoArray(value string, bits uint8) []byte {
	i, _ := strconv.Atoi(value)

	if bits == 8 {
		bytes := make([]byte, 1)
		bytes[0] = uint8(i & 0xFF)
		return bytes

	} else if bits == 16 {
		bytes := make([]byte, 2)
		bytes[0] = uint8((i >> 8) & 0xFF)
		bytes[1] = uint8(i & 0xFF)
		return bytes

	} else if bits == 32 {
		bytes := make([]byte, 4)
		bytes[0] = uint8((i >> 24) & 0xFF)
		bytes[1] = uint8((i >> 16) & 0xFF)
		bytes[2] = uint8((i >> 8) & 0xFF)
		bytes[3] = uint8(i & 0xFF)
		return bytes
	}

	return nil
}
