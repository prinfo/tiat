package dhcp

import (
	"bytes"
	"database/sql"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
	"time"

	"gitlab.com/prinfo/tiat/metric"
	"golang.org/x/time/rate"
)

//Server implements Tiat DHCP server.
type Server struct {
	APITokens        []string
	DBHost           string
	conns            []*net.UDPConn
	dhcpServices     []*service
	limiter          *rate.Limiter
	psql             *sql.DB
	serverAddresses  []net.IP
	pauseClearTicker chan bool
}

//Test creates a copy of Server and tests the configuration
func Test(psql *sql.DB) error {
	t := Server{
		psql: psql,
	}

	if err := t.services(); err != nil {
		return err
	}

	t.dhcpServices = nil
	return nil
}

//Loads services in memory
func (s *Server) load() error {
	var err error

	if err = s.services(); err != nil {
		return err
	}

	/*If a pool has been made smaller we need to remove the existing
	  leases that are out of range*/
	if err = s.removeInvalidLeases(); err != nil {
		return err
	}

	//Find existing leases and make them unavailable
	if err = s.removeExistingLeases(); err != nil {
		return err
	}

	return nil
}

/*Start the DHCP server.

address is the IP the server will listen to and send as indentifier.

db is the backend connection to PostgreSQL.
*/
func (s *Server) Start(addresses []net.IP, db *sql.DB) error {
	fmt.Println("DHCP Starting...")

	if addresses == nil {
		return errors.New("addresses can not be nil for dhcp Server.Start()")
	}

	if db == nil {
		return errors.New("db can not be nil for dhcp Server.Start()")
	}

	s.pauseClearTicker = make(chan bool)
	s.serverAddresses = addresses
	s.psql = db

	//Limiter used by Reload()
	s.limiter = rate.NewLimiter(rate.Every(10*time.Second), 1)

	if err := s.load(); err != nil {
		log.Println("DHCP Failed to Start.", err)

	} else {
		for i, address := range s.serverAddresses {
			var err error
			s.conns = append(s.conns, &net.UDPConn{})
			s.conns[i], err = net.ListenUDP("udp", &net.UDPAddr{IP: address, Port: 67})
			if err != nil {
				return err
			}

			go s.processDhcp(s.conns[i])
		}

		fmt.Println("DHCP Started")
	}

	s.startClearLeaseTimer()

	//If Metrics is enabled start a timer to check the number of active leases
	if metric.Enabled {
		leaseCountTimer := time.NewTicker(10 * time.Second)
		go func() {
			for {
				<-leaseCountTimer.C
				for k, v := range s.leaseCount() {
					metric.Write("dhcp_leases", map[string]string{"subnet": k}, map[string]interface{}{"leases": v}, s.DBHost)
				}
			}
		}()
	}

	return nil
}

//startClearLeaseTimer starts a ticker to clear old leases every 5 seconds
func (s *Server) startClearLeaseTimer() {
	leaseTimer := time.NewTicker(5 * time.Second)
	go func() {
		for {
			select {
			case <-s.pauseClearTicker:
				leaseTimer.Stop()
				return
			case <-leaseTimer.C:
				s.clearOldLeases()
			}
		}
	}()
}

//processDhcp reads incoming DHCP packets
func (s *Server) processDhcp(conn *net.UDPConn) {
	defer conn.Close()
	serverIP := strings.Split(conn.LocalAddr().String(), ":")[0]

MAINLOOP:
	for {
		buffer := make([]byte, 1024)
		size, sourceIP, err := conn.ReadFromUDP(buffer)
		if err != nil {
			if strings.HasSuffix(err.Error(), "use of closed network connection") {
				return
			}

			log.Println(err)
			continue
		}
		reader := bytes.NewReader(buffer[:size])

		time := time.Now()

		//Read the DHCP header
		var header dhcpHeader
		if err := binary.Read(reader, binary.BigEndian, &header); err != nil {
			log.Println(err)
			continue
		}

		//Read the remaining data to dhcpOptions
		options := dhcpOptions{}
		options.data = make([]byte, reader.Len())
		reader.Read(options.data)

		option82 := options.option82()
		chAddr := net.HardwareAddr(header.ChAddr[:])
		ciAddr := net.IP(header.CiAddr[:])
		msg := options.optionValue(53)
		if msg == nil {
			fmt.Println("Unknown message type from (" + chAddr.String() + ")")
			continue
		}
		var requestedIP net.IP

		for i := range s.dhcpServices {
			agent := net.ParseIP(s.dhcpServices[i].RelayAgent).To4()
			if agent.Equal(header.GiAddr[:]) {
				switch msg[0] {
				case Discover:
					fmt.Println("DHCPDiscover", option82, "("+chAddr.String()+")")

					go metric.Write("dhcp_types", nil, map[string]interface{}{"discover": 1}, serverIP)
					requestedIP = net.IP(options.optionValue(50))
					s.offer(conn, &header, option82, s.dhcpServices[i], requestedIP)
					continue MAINLOOP

				case Request:
					if ciAddr.IsUnspecified() {
						requestedIP = net.IP(options.optionValue(50))

					} else {
						requestedIP = ciAddr
					}

					fmt.Println("DHCPRequest", requestedIP, "from", option82, "("+chAddr.String()+")")

					go metric.Write("dhcp_types", nil, map[string]interface{}{"request": 1}, serverIP)
					s.ack(conn, &header, option82, s.dhcpServices[i], requestedIP, time)
					continue MAINLOOP

				case Decline:

					if ciAddr.IsUnspecified() {
						requestedIP = net.IP(options.optionValue(50))
					} else {
						requestedIP = ciAddr
					}

					fmt.Println("DHCPDecline", requestedIP, "from", option82, "("+chAddr.String()+")")
					go metric.Write("dhcp_types", nil, map[string]interface{}{"decline": 1}, serverIP)

					if err := s.removeLease(requestedIP, chAddr); err != nil {
						log.Println(err)
					}
					continue MAINLOOP

				case Inform:
					//TODO
					fmt.Println("Inform from", option82, "("+chAddr.String()+")")
					go metric.Write("dhcp_types", nil, map[string]interface{}{"inform": 1}, serverIP)
					continue MAINLOOP
				}
			}
		}

		/*If it's a unicast renewal the giAddr is not set but ciAddr is, and the
		server should reply directly to the client*/
		if msg[0] == Request && !ciAddr.IsUnspecified() {
			fmt.Println("DHCPRequest", ciAddr.String(), "from", option82, "("+chAddr.String()+")")
			go metric.Write("dhcp_types", nil, map[string]interface{}{"request": 1}, serverIP)
			s.ackRenew(conn, &header, option82, sourceIP.IP, net.IP(options.optionValue(50)), time)

		} else if msg[0] == Release && !ciAddr.IsUnspecified() {
			fmt.Println("DHCPRelease", ciAddr.String(), "from", option82, "("+chAddr.String()+")")
			go metric.Write("dhcp_types", nil, map[string]interface{}{"release": 1}, serverIP)

			if err := s.removeLease(ciAddr, chAddr); err != nil {
				log.Println(err)
			}

		} else {
			fmt.Println("No matching service for", option82, "("+chAddr.String()+")")
		}
	}
}

//offer creates a DHCPOffer packet and sends it to the relay agent
func (s *Server) offer(conn *net.UDPConn, header *dhcpHeader, option82 string, service *service, requestedIP net.IP) {

	mac := net.HardwareAddr(header.ChAddr[:])
	knownClient, clientSubnet, err := s.knownClient(mac, option82, service.ID)
	if err != nil {
		log.Println(err)
		return
	}

	clientIP, err := s.lease(mac, 0)
	if err != nil {
		log.Println(err)
		return
	}

	var options []byte
	if clientIP != nil {
		for _, subnet := range service.subnets {
			if subnet.cidr.Contains(clientIP) {
				//If a client has changed known status
				if (subnet.AllowUnknown && knownClient > 0) ||
					(!subnet.AllowUnknown && knownClient == 0) {
					clientIP = nil
					break
				} else {
					options = append(options, subnet.options.data...)
					break
				}
			}
		}
	}

	if clientIP == nil {
		var staticIP net.IP
		if knownClient > 0 {
			staticIP, err = s.staticIP(knownClient)
			if err != nil {
				log.Println(err)
				return
			}
		}

		for i, subnet := range service.subnets {

			//If the client is configured for a specific subnet we skip the others
			if clientSubnet > 0 && subnet.ID != clientSubnet {
				continue
			}

			if len(subnet.pool.ips) == 0 {
				if i == len(service.subnets)-1 {
					fmt.Println(service.Service, "is out of available IPs")
					return
				}
				continue

			} else if !subnet.AllowUnknown && knownClient > 0 {
				if staticIP != nil || subnet.IsStatic {
					if staticIP == nil {
						continue
					}

					isIPAvailable, err := s.leaseAvailable(staticIP)
					if err != nil {
						log.Println(err)
						return
					}

					if subnet.cidr.Contains(staticIP) && isIPAvailable {
						clientIP = staticIP
						if err := s.addLease(clientIP, mac, option82, subnet.ID); err != nil {
							log.Println(err)
							return
						}

						options = append(options, subnet.options.data...)
						break
					}

				} else {
					ipCount, err := s.clientIPCount(knownClient)
					if err != nil {
						log.Println(err)
						return
					}

					var leases uint32
					if clientSubnet > 0 {
						leases, err = s.clientSubnetLeases(option82, clientSubnet)
						if err != nil {
							log.Println(err)
							return
						}

					} else {
						leases, err = s.clientServiceLeases(option82, service.ID)
						if err != nil {
							log.Println(err)
							return
						}
					}

					if ipCount > leases {
						if requestedIP == nil || requestedIP.IsUnspecified() {
							clientIP = *subnet.pool.ips[0]

						} else {
							isIPAvailable, err := s.leaseAvailable(requestedIP)
							if err != nil {
								log.Println(err)
								return
							}

							if isIPAvailable && withinPool(subnet.pool.ips, requestedIP) {
								clientIP = requestedIP

							} else {
								clientIP = *subnet.pool.ips[0]
							}
						}

						if err := s.addLease(clientIP, mac, option82, subnet.ID); err != nil {
							log.Println(err)
							return
						}

						options = append(options, subnet.options.data...)
						break
					}

					fmt.Println("Max IP count reached for", option82)
					return
				}

			} else if subnet.AllowUnknown && knownClient == 0 {
				if service.ClientLimit > 0 {
					leases, err := s.clientServiceLeases(option82, service.ID)
					if err != nil {
						log.Println(err)
						return
					}

					if leases >= uint32(service.ClientLimit) {
						fmt.Println("Max IP count reached for", option82, "in service", service.Service)
						return
					}

					if requestedIP == nil || requestedIP.IsUnspecified() {
						clientIP = *subnet.pool.ips[0]

					} else {
						isIPAvailable, err := s.leaseAvailable(requestedIP)
						if err != nil {
							log.Println(err)
							return
						}

						if isIPAvailable && withinPool(subnet.pool.ips, requestedIP) {
							clientIP = requestedIP

						} else {
							clientIP = *subnet.pool.ips[0]
						}
					}

					if err := s.addLease(clientIP, mac, option82, subnet.ID); err != nil {
						log.Println(err)
						return
					}

					options = append(options, subnet.options.data...)
					break

				} else {

					if requestedIP == nil || requestedIP.IsUnspecified() {
						clientIP = *subnet.pool.ips[0]

					} else {
						isIPAvailable, err := s.leaseAvailable(requestedIP)
						if err != nil {
							log.Println(err)
							return
						}

						if isIPAvailable && withinPool(subnet.pool.ips, requestedIP) {
							clientIP = requestedIP

						} else {
							clientIP = *subnet.pool.ips[0]
						}
					}

					/*If a client is offered an IP from an unrestricted subnet
					  we don't add the option82 value to the lease database*/
					if err := s.addLease(clientIP, mac, "", subnet.ID); err != nil {
						log.Println(err)
						return
					}

					options = append(options, subnet.options.data...)
					break
				}
			}

			if i == len(service.subnets)-1 {
				fmt.Println("No available IP in", service.Service, "for", option82, "("+mac.String()+")")
				return
			}
		}
	}

	if knownClient > 0 {
		clientOptions, err := s.clientOptions(knownClient)
		if err != nil {
			log.Println(err)
			return
		}
		options = addClientOptions(options, clientOptions)
	}

	serverIP := strings.Split(conn.LocalAddr().String(), ":")[0]
	var offer []byte
	offer = append(offer, 2) //OP Code
	offer = append(offer, header.HardwareType)
	offer = append(offer, header.HardwareLen)
	offer = append(offer, header.Hops)
	offer = append(offer, header.TransactionID[:]...)
	offer = append(offer, 0, 0) //Secs
	offer = append(offer, header.Flags[:]...)
	offer = append(offer, 0, 0, 0, 0) //ciAddr
	offer = append(offer, clientIP.To4()...)
	offer = append(offer, 0, 0, 0, 0) //siAddr
	offer = append(offer, header.GiAddr[:]...)
	offer = append(offer, mac...)
	offer = append(offer, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) //Padding
	offer = append(offer, header.ServerName[:]...)
	offer = append(offer, header.BootFile[:]...)
	offer = append(offer, header.Cookie[:]...)
	offer = append(offer, 53) //Option 53 (Msg Type)
	offer = append(offer, 1)  //Length
	offer = append(offer, 2)  //Msg (Offer)
	offer = append(offer, 54) //Option 54 (Service identifier)
	offer = append(offer, 4)  //Length
	offer = append(offer, net.ParseIP(serverIP).To4()...)
	offer = append(offer, options...)
	offer = append(offer, 255) //End option

	if len(offer) < 300 {
		//Add padding
		offer = append(offer, make([]byte, 300-len(offer))...)
	}

	conn.WriteToUDP(offer, &net.UDPAddr{IP: header.GiAddr[:], Port: 67})
	fmt.Println("DHCPOffer", clientIP, "to", option82, "("+mac.String()+")")
	go metric.Write("dhcp_types", nil, map[string]interface{}{"offer": 1}, serverIP)
}

//ack creates a DHCP Ack packet and sends it to the relay agent
func (s *Server) ack(conn *net.UDPConn, header *dhcpHeader, option82 string, service *service, requestedIP net.IP, startTime time.Time) {

	mac := net.HardwareAddr(header.ChAddr[:])
	knownClient, clientSubnet, err := s.knownClient(mac, option82, service.ID)
	if err != nil {
		log.Println(err)
		return
	}

	if knownClient > 0 {
		ipCount, err := s.clientIPCount(knownClient)
		if err != nil {
			log.Println(err)
			return
		}

		var leases uint32
		if clientSubnet > 0 {
			leases, err = s.clientSubnetLeases(option82, clientSubnet)
			if err != nil {
				log.Println(err)
				return
			}

		} else {
			leases, err = s.clientServiceLeases(option82, service.ID)
			if err != nil {
				log.Println(err)
				return
			}
		}

		if ipCount < leases {
			s.nak(conn, header, option82, net.IP(header.GiAddr[:]), 67, requestedIP)
			return
		}
	}

	var options []byte
	for _, subnet := range service.subnets {

		//If the client is configured for a specific subnet we skip the others
		if clientSubnet > 0 && subnet.ID != clientSubnet {
			continue
		}

		/*Continue to the next subnet if the subnet doesn't allow unknown clients
		  and the client is unknown*/
		if (subnet.AllowUnknown && knownClient > 0) ||
			(!subnet.AllowUnknown && knownClient == 0) {
			continue
		}

		clientIP, err := s.lease(mac, subnet.ID)
		if err != nil {
			log.Println(err)
			return
		}

		if subnet.cidr.Contains(clientIP) {
			staticIP, err := s.staticIP(knownClient)
			if err != nil {
				log.Println(err)
				return
			}

			if (subnet.IsStatic && staticIP == nil) ||
				(!subnet.IsStatic && staticIP != nil) {
				break
			}

			options = append(options, subnet.options.data...)

			if err := s.updateLease(mac, clientIP); err != nil {
				log.Println(err)
				return
			}

			if knownClient > 0 {
				clientOptions, err := s.clientOptions(knownClient)
				if err != nil {
					log.Println(err)
					return
				}
				options = addClientOptions(options, clientOptions)
			}

			serverIP := strings.Split(conn.LocalAddr().String(), ":")[0]
			var ack []byte
			ack = append(ack, 2) //OP Code
			ack = append(ack, header.HardwareType)
			ack = append(ack, header.HardwareLen)
			ack = append(ack, header.Hops)
			ack = append(ack, header.TransactionID[:]...)
			ack = append(ack, 0, 0) //Secs
			ack = append(ack, header.Flags[:]...)
			ack = append(ack, 0, 0, 0, 0) //ciAddr
			ack = append(ack, clientIP.To4()...)
			ack = append(ack, 0, 0, 0, 0) //siAddr
			ack = append(ack, header.GiAddr[:]...)
			ack = append(ack, mac...)
			ack = append(ack, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) //Padding
			ack = append(ack, header.ServerName[:]...)
			ack = append(ack, header.BootFile[:]...)
			ack = append(ack, header.Cookie[:]...)
			ack = append(ack, 53) //Option 53 (Msg Type)
			ack = append(ack, 1)  //Length
			ack = append(ack, 5)  //Msg (Ack)
			ack = append(ack, 54) //Option 54 (Service identifier)
			ack = append(ack, 4)  //Length
			ack = append(ack, net.ParseIP(serverIP).To4()...)
			ack = append(ack, options...)
			ack = append(ack, 255) //End option

			if len(ack) < 300 {
				//Add padding
				ack = append(ack, make([]byte, 300-len(ack))...)
			}

			conn.WriteToUDP(ack, &net.UDPAddr{IP: header.GiAddr[:], Port: 67})
			fmt.Println("DHCPAck", clientIP, "to", option82, "("+mac.String()+")")
			go metric.Write("dhcp_types", nil, map[string]interface{}{"ack": 1}, serverIP)
			go metric.Write("dhcp_metric", nil, map[string]interface{}{"response_time_ms": time.Since(startTime).Milliseconds()}, serverIP)

			return
		}
	}

	s.nak(conn, header, option82, net.IP(header.GiAddr[:]), 67, requestedIP)
}

//ackRenew creates a DHCP Ack packet and sends it to the client
func (s *Server) ackRenew(conn *net.UDPConn, header *dhcpHeader, option82 string, sourceIP, requestedIP net.IP, startTime time.Time) {

	ciAddr := net.IP(header.CiAddr[:])
	serviceID, err := s.serviceID(ciAddr)
	if err != nil {
		log.Println(err)
		return
	}

	if serviceID < 1 {
		s.nak(conn, header, option82, sourceIP, 68, requestedIP)
		return
	}

	var service *service
	for _, s := range s.dhcpServices {
		if s.ID == serviceID {
			service = s
			break
		}
	}

	mac := net.HardwareAddr(header.ChAddr[:])
	knownClient, clientSubnet, err := s.knownClient(mac, option82, service.ID)
	if err != nil {
		log.Println(err)
		return
	}

	if knownClient > 0 {
		ipCount, err := s.clientIPCount(knownClient)
		if err != nil {
			log.Println(err)
			return
		}

		var leases uint32
		if clientSubnet > 0 {
			leases, err = s.clientSubnetLeases(option82, clientSubnet)
			if err != nil {
				log.Println(err)
				return
			}

		} else {
			leases, err = s.clientServiceLeases(option82, service.ID)
			if err != nil {
				log.Println(err)
				return
			}
		}

		if ipCount < leases {
			s.nak(conn, header, option82, sourceIP, 68, requestedIP)
			return
		}
	}

	var options []byte
	for _, subnet := range service.subnets {

		//If the client is configured for a specific subnet we skip the others
		if clientSubnet > 0 && subnet.ID != clientSubnet {
			continue
		}

		/*Continue to the next subnet if the subnet doesn't allow unknown clients
		  and the client is unknown*/
		if (subnet.AllowUnknown && knownClient > 0) ||
			(!subnet.AllowUnknown && knownClient == 0) {
			continue
		}

		clientIP, err := s.lease(mac, subnet.ID)
		if err != nil {
			log.Println(err)
			return
		}

		if subnet.cidr.Contains(clientIP) {
			staticIP, err := s.staticIP(knownClient)
			if err != nil {
				log.Println(err)
				return
			}

			if (subnet.IsStatic && staticIP == nil) ||
				(!subnet.IsStatic && staticIP != nil) {
				break
			}

			options = append(options, subnet.options.data...)

			if err := s.updateLease(mac, clientIP); err != nil {
				log.Println(err)
				return
			}

			if knownClient > 0 {
				clientOptions, err := s.clientOptions(knownClient)
				if err != nil {
					log.Println(err)
					return
				}
				options = addClientOptions(options, clientOptions)
			}

			serverIP := strings.Split(conn.LocalAddr().String(), ":")[0]
			var ack []byte
			ack = append(ack, 2) //OP Code
			ack = append(ack, header.HardwareType)
			ack = append(ack, header.HardwareLen)
			ack = append(ack, header.Hops)
			ack = append(ack, header.TransactionID[:]...)
			ack = append(ack, 0, 0) //Secs
			ack = append(ack, header.Flags[:]...)
			ack = append(ack, header.CiAddr[:]...)
			ack = append(ack, clientIP.To4()...)
			ack = append(ack, 0, 0, 0, 0) //siAddr
			ack = append(ack, 0, 0, 0, 0) //GiAddr
			ack = append(ack, mac...)
			ack = append(ack, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) //Padding
			ack = append(ack, header.ServerName[:]...)
			ack = append(ack, header.BootFile[:]...)
			ack = append(ack, header.Cookie[:]...)
			ack = append(ack, 53) //Option 53 (Msg Type)
			ack = append(ack, 1)  //Length
			ack = append(ack, 5)  //Msg (Ack)
			ack = append(ack, 54) //Option 54 (Service identifier)
			ack = append(ack, 4)  //Length
			ack = append(ack, net.ParseIP(serverIP).To4()...)
			ack = append(ack, options...)
			ack = append(ack, 255) //End option

			if len(ack) < 300 {
				//Add padding
				ack = append(ack, make([]byte, 300-len(ack))...)
			}

			conn.WriteToUDP(ack, &net.UDPAddr{IP: sourceIP, Port: 68})
			fmt.Println("DHCPAck", clientIP, "to", option82, "("+mac.String()+")")
			go metric.Write("dhcp_types", nil, map[string]interface{}{"ack": 1}, serverIP)
			go metric.Write("dhcp_metric", nil, map[string]interface{}{"response_time_ms": time.Since(startTime).Milliseconds()}, serverIP)

			return
		}
	}
	s.nak(conn, header, option82, sourceIP, 68, requestedIP)
}

//nak creates a DHCP Nak packet and sends it to the client
func (s *Server) nak(conn *net.UDPConn, header *dhcpHeader, option82 string, sourceIP net.IP, port uint16, requestedIP net.IP) {

	mac := net.HardwareAddr(header.ChAddr[:])
	serverIP := strings.Split(conn.LocalAddr().String(), ":")[0]

	var nak []byte
	nak = append(nak, 2) //OP Code
	nak = append(nak, header.HardwareType)
	nak = append(nak, header.HardwareLen)
	nak = append(nak, header.Hops)
	nak = append(nak, header.TransactionID[:]...)
	nak = append(nak, 0, 0) //Secs
	nak = append(nak, header.Flags[:]...)
	nak = append(nak, 0, 0, 0, 0) //ciAddr
	nak = append(nak, 0, 0, 0, 0) //yiAddr
	nak = append(nak, 0, 0, 0, 0) //siAddr
	nak = append(nak, header.GiAddr[:]...)
	nak = append(nak, mac...)
	nak = append(nak, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) //Padding
	nak = append(nak, header.ServerName[:]...)
	nak = append(nak, header.BootFile[:]...)
	nak = append(nak, header.Cookie[:]...)
	nak = append(nak, 53) //Option 53 (Msg Type)
	nak = append(nak, 1)  //Length
	nak = append(nak, 6)  //Msg (Nak)
	nak = append(nak, 54) //Option 54 (Service identifier)
	nak = append(nak, 4)  //Length
	nak = append(nak, net.ParseIP(serverIP).To4()...)
	nak = append(nak, 255) //End option

	if len(nak) < 300 {
		//Add padding
		nak = append(nak, make([]byte, 300-len(nak))...)
	}

	var ip net.IP
	if net.IP(header.CiAddr[:]).IsUnspecified() {
		ip = requestedIP

	} else {
		ip = net.IP(header.CiAddr[:])
	}

	if err := s.removeLease(ip, mac); err != nil {
		log.Println(err)
	}

	conn.WriteToUDP(nak, &net.UDPAddr{IP: sourceIP, Port: int(port)})
	fmt.Println("DHCPNak", ip, "for", option82, "("+mac.String()+")")
	go metric.Write("dhcp_types", nil, map[string]interface{}{"nak": 1}, serverIP)
}

//services returns all rows from dhcp4_services and adds to Dhcp.dhcpServices
func (s *Server) services() error {
	rows, err := s.psql.Query("SELECT id, service, relayagent, clientlimit FROM dhcp4_services")
	if err != nil {
		return err
	}
	defer rows.Close()

	services := []*service{}
	for rows.Next() {
		service := service{}
		err := rows.Scan(&service.ID, &service.Service, &service.RelayAgent, &service.ClientLimit)
		if err != nil {
			return err
		}

		service.subnets, err = s.subnets(service.ID)
		if err != nil {
			return err
		}

		services = append(services, &service)
	}

	s.dhcpServices = services

	err = rows.Err()
	if err != nil {
		return err
	}

	return nil
}

//subnets returns all subnets based on given serviceID
func (s *Server) subnets(serviceID uint32) ([]*subnet, error) {
	rows, err := s.psql.Query("SELECT id, subnet, allowunknown, isstatic FROM dhcp4_subnets WHERE service = $1", serviceID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var subnets []*subnet
	for rows.Next() {
		subnet := subnet{}
		err := rows.Scan(&subnet.ID, &subnet.Subnet, &subnet.AllowUnknown, &subnet.IsStatic)
		if err != nil {
			return nil, err
		}

		_, subnet.cidr, _ = net.ParseCIDR(subnet.Subnet)
		if err := s.subnetExists(subnet.cidr); err != nil {
			return nil, err
		}

		subnet.pool, err = s.pool(subnet.ID)
		if err != nil {
			return nil, err
		}

		subnet.options, err = s.options(subnet.ID)
		if err != nil {
			return nil, err
		}

		subnets = append(subnets, &subnet)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return subnets, nil
}

//options returns the pool options for the given subnet
func (s *Server) options(subnetID uint32) (dhcpOptions, error) {
	var options dhcpOptions
	rows, err := s.psql.Query("SELECT option, value FROM dhcp4_pooloptions WHERE subnet = $1", subnetID)
	if err != nil {
		return options, err
	}
	defer rows.Close()

	//Look for vendor options separately
	vendorOptions, err := s.vendorOptions(subnetID)
	if err != nil {
		return options, err
	}
	options.data = append(options.data, vendorOptions...)

	for rows.Next() {
		var option uint8
		var value []byte
		err := rows.Scan(&option, &value)
		if err != nil {
			return options, err
		}

		//Skip options 43, we've already added them
		if option == 43 {
			continue
		}

		options.addOption(option, value)
	}

	if err := rows.Err(); err != nil {
		return options, err
	}

	return options, nil
}

//vendorOptions returns a []byte from dhcp4_vendoroptions
func (s *Server) vendorOptions(subnetID uint32) ([]byte, error) {
	rows, err := s.psql.Query(`SELECT suboption, value, type FROM dhcp4_vendoroptions WHERE name IN
	(SELECT value FROM dhcp4_pooloptions WHERE option = 43 AND subnet = $1)`, subnetID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var vendorOptions dhcpOptions
	for rows.Next() {
		var subOption uint8
		var value []byte
		var vendorType uint8
		err := rows.Scan(&subOption, &value, &vendorType)
		if err != nil {
			return nil, err
		}

		vendorOptions.data = append(vendorOptions.data, subOption)

		switch vendorType {
		case 1, 7: //string or hex
			vendorOptions.data = append(vendorOptions.data, uint8(len(value)))
			vendorOptions.data = append(vendorOptions.data, value...)
		case 2: //uint8
			vendorOptions.data = append(vendorOptions.data, 1)
			vendorOptions.data = append(vendorOptions.data, vendorOptions.intStringtoArray(string(value), 8)...)
		case 3: //uint16
			vendorOptions.data = append(vendorOptions.data, 2)
			vendorOptions.data = append(vendorOptions.data, vendorOptions.intStringtoArray(string(value), 16)...)
		case 4: //uint32
			vendorOptions.data = append(vendorOptions.data, 4)
			vendorOptions.data = append(vendorOptions.data, vendorOptions.intStringtoArray(string(value), 32)...)
		case 5: //IP Address
			vendorOptions.data = append(vendorOptions.data, 4)
			vendorOptions.data = append(vendorOptions.data, net.ParseIP(string(value)).To4()...)
		case 6: //Bool
			vendorOptions.data = append(vendorOptions.data, 1)
			b, _ := strconv.ParseBool(string(value))
			if b {
				vendorOptions.data = append(vendorOptions.data, 1)
			} else {
				vendorOptions.data = append(vendorOptions.data, 0)
			}
		}
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	totalSize := uint8(len(vendorOptions.data))
	if totalSize == 0 {
		return nil, nil
	}

	/*Insert the main option and total size at the beginning. The value 43 will be at index 0
	and the size at index 1*/
	vendorOptions.data = append([]uint8{totalSize}, vendorOptions.data...)
	vendorOptions.data = append([]uint8{43}, vendorOptions.data...)

	return vendorOptions.data, nil
}

//pool returns the subnet's IP pool
func (s *Server) pool(subnetID uint32) (pool, error) {
	row := s.psql.QueryRow(`SELECT poolstart, poolend FROM dhcp4_pools WHERE
		poolstart << (SELECT subnet FROM dhcp4_subnets WHERE id = $1) AND
		poolend << (SELECT subnet FROM dhcp4_subnets WHERE id = $1)`, subnetID)

	pool := pool{}
	err := row.Scan(&pool.Start, &pool.End)
	if err != nil {
		if err == sql.ErrNoRows {
			return pool, errors.New("Pool is empty or out of scope for subnet ID " + strconv.Itoa(int(subnetID)))
		}
		return pool, err
	}

	//Convert IPs to uint32
	start := binary.BigEndian.Uint32(net.ParseIP(pool.Start).To4())
	end := binary.BigEndian.Uint32(net.ParseIP(pool.End).To4())

	for i := start; i <= end; i++ {
		ip := make(net.IP, 4)
		binary.BigEndian.PutUint32(ip, i)
		pool.ips = append(pool.ips, &ip)
	}

	return pool, nil
}

//subnetExists check if the subnet contains or is contained within another subnet.
func (s *Server) subnetExists(subnet *net.IPNet) error {
	row := s.psql.QueryRow("SELECT subnet FROM dhcp4_subnets WHERE $1 >> subnet OR $1 << subnet", subnet.String())

	if err := row.Scan(); err != sql.ErrNoRows {
		return errors.New("The subnet " + subnet.String() + " contains or is contained within another subnet")
	}

	return nil
}

//serviceID returns the service ID bases on the given ciAddr
func (s *Server) serviceID(ciAddr net.IP) (uint32, error) {
	row := s.psql.QueryRow("SELECT service FROM dhcp4_subnets WHERE $1 << subnet", ciAddr.String())

	var serviceID uint32
	err := row.Scan(&serviceID)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}
		return 0, err
	}

	return serviceID, nil
}

//clearOldLeases is called every 5 sec to clear leases older than twice their lease time
func (s *Server) clearOldLeases() {
	rows, err := s.psql.Query(`DELETE FROM dhcp4_leases WHERE
		leasetime < current_timestamp - 2 *
		(SELECT value FROM dhcp4_pooloptions WHERE option = 51 AND
		subnet = dhcp4_leases.subnet)::INTERVAL RETURNING ipaddress, subnet`)
	if err != nil {
		log.Println(err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var lease string
		var subnetID uint32
		err := rows.Scan(&lease, &subnetID)
		if err != nil {
			log.Println(err)
			return
		}

		ip := net.ParseIP(lease)
		s.restoreIP(&ip, subnetID)
	}

	if err := rows.Err(); err != nil {
		log.Println(err)
		return
	}
}

//removeInvalidLeases removes leases out of range if a pool has changed
func (s *Server) removeInvalidLeases() error {
	_, err := s.psql.Exec(`DELETE FROM dhcp4_leases where ipaddress IN
		(SELECT ipaddress FROM dhcp4_leases JOIN dhcp4_pools ON
		dhcp4_leases.subnet = dhcp4_pools.subnet WHERE
		dhcp4_leases.ipaddress < dhcp4_pools.poolstart OR
		dhcp4_leases.ipaddress > dhcp4_pools.poolend)`)
	if err != nil {
		return err
	}

	return nil
}

//removeExistingLeases finds existing leases and remove them from the pools
func (s *Server) removeExistingLeases() error {
	rows, err := s.psql.Query("SELECT ipaddress, subnet FROM dhcp4_leases")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var lease string
		var subnetID uint32

		err := rows.Scan(&lease, &subnetID)
		if err != nil {
			return err
		}

		ip := net.ParseIP(lease)
		s.removeIP(ip, subnetID)
	}

	if err := rows.Err(); err != nil {
		return err
	}

	return nil
}

//restoreIP restores an IP to the subnet's pool
func (s *Server) restoreIP(ip *net.IP, subnetID uint32) {
	for _, services := range s.dhcpServices {
		for _, subnets := range services.subnets {
			if subnets.ID == subnetID {
				subnets.pool.ips = append(subnets.pool.ips, ip)
				return
			}
		}
	}
}

//removeIP deletes an IP from the subnet's pool
func (s *Server) removeIP(ip net.IP, subnetID uint32) {
	for _, services := range s.dhcpServices {
		for _, subnets := range services.subnets {
			if subnets.ID == subnetID {
				for i, lease := range subnets.pool.ips {
					if lease.Equal(ip) {
						copy(subnets.pool.ips[i:], subnets.pool.ips[i+1:])
						subnets.pool.ips[len(subnets.pool.ips)-1] = nil
						subnets.pool.ips = subnets.pool.ips[:len(subnets.pool.ips)-1]
						return
					}
				}
			}
		}
	}
}

/*knownClient returns the ID and subnet of the known configured client.
Mac address has higher priority than option82*/
func (s *Server) knownClient(mac net.HardwareAddr, option82 string, serviceID uint32) (uint32, uint32, error) {
	var id uint32
	var subnet sql.NullInt32

	row := s.psql.QueryRow("SELECT id, subnet FROM dhcp4_knownclients WHERE value = $1 AND service = $2", mac.String(), serviceID)
	err := row.Scan(&id, &subnet)
	if err != nil && err != sql.ErrNoRows {
		return 0, 0, err
	}

	if id > 0 {
		return id, uint32(subnet.Int32), nil
	}

	row = s.psql.QueryRow("SELECT id, subnet FROM dhcp4_knownclients WHERE value = $1 AND service = $2", option82, serviceID)
	err = row.Scan(&id, &subnet)
	if err != nil && err != sql.ErrNoRows {
		return 0, 0, err
	}

	return id, uint32(subnet.Int32), nil
}

//staticIP checks if the known client is configured with a specific IP and returns it
func (s *Server) staticIP(clientID uint32) (net.IP, error) {
	row := s.psql.QueryRow("SELECT ipaddress FROM dhcp4_staticips WHERE client = $1", clientID)

	var address string
	err := row.Scan(&address)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	return net.ParseIP(address), nil
}

//lease returns the leased IP
func (s *Server) lease(mac net.HardwareAddr, subnetID uint32) (net.IP, error) {
	var row *sql.Row
	if subnetID > 0 {
		row = s.psql.QueryRow("SELECT ipaddress FROM dhcp4_leases WHERE macaddress = $1 AND subnet = $2", mac.String(), subnetID)

	} else {
		row = s.psql.QueryRow("SELECT ipaddress FROM dhcp4_leases WHERE macaddress = $1", mac.String())
	}

	var address string
	err := row.Scan(&address)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	return net.ParseIP(address), nil
}

//leaseCount returns the number of active leases per subnet
func (s *Server) leaseCount() map[string]int32 {
	rows, _ := s.psql.Query(`SELECT dhcp4_subnets.subnet, COUNT(dhcp4_leases.id) FROM dhcp4_leases JOIN 
	dhcp4_subnets ON dhcp4_leases.subnet = dhcp4_subnets.id GROUP BY dhcp4_subnets.id`)
	defer rows.Close()

	values := make(map[string]int32)
	for rows.Next() {
		var subnet string
		var count int32
		rows.Scan(&subnet, &count)

		values[subnet] = count
	}

	return values
}

//leaseAvailable checks if the IP is leased or not
func (s *Server) leaseAvailable(ip net.IP) (bool, error) {
	row := s.psql.QueryRow("SELECT exists(SELECT 1 FROM dhcp4_leases WHERE ipaddress = $1)", ip.String())

	var exists bool
	err := row.Scan(&exists)
	if err != nil {
		return false, err
	}

	if !exists {
		return true, nil
	}

	return false, nil
}

//addLease adds a lease to dhcp4_leases and removes the IP from the subnet's pool
func (s *Server) addLease(ip net.IP, mac net.HardwareAddr, option82 string, subnetID uint32) error {
	_, err := s.psql.Exec("INSERT INTO dhcp4_leases (ipaddress, macaddress, option82, subnet) values ($1, $2, $3, $4)",
		ip.String(), mac.String(), option82, subnetID)
	if err != nil {
		return err
	}

	s.removeIP(ip, subnetID)

	return nil
}

//removeLease deletes a lease from dhcp4_leases and restores the IP to the subnet's pool
func (s *Server) removeLease(lease net.IP, mac net.HardwareAddr) error {
	row := s.psql.QueryRow("DELETE FROM dhcp4_leases WHERE ipaddress = $1 AND macaddress = $2 RETURNING ipaddress, subnet",
		lease.String(), mac.String())

	var address string
	var subnetID uint32
	err := row.Scan(&address, &subnetID)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
		return err
	}

	ip := net.ParseIP(address)
	s.restoreIP(&ip, subnetID)

	return nil
}

//updateLease updates the timestamp on the given mac
func (s *Server) updateLease(mac net.HardwareAddr, ip net.IP) error {
	_, err := s.psql.Exec("UPDATE dhcp4_leases SET leasetime = current_timestamp WHERE macaddress = $1 AND ipaddress = $2",
		mac.String(), ip.String())
	if err != nil {
		return err
	}

	return nil
}

//clientIPCount returns how many IPs the known client is allowed
func (s *Server) clientIPCount(id uint32) (uint32, error) {
	row := s.psql.QueryRow("SELECT ipcount FROM dhcp4_knownclients WHERE id = $1", id)

	var count uint32
	err := row.Scan(&count)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}

		return 0, err
	}

	return count, nil
}

//clientServiceLeases returns how many leases that are in use for the option82 in given service
func (s *Server) clientServiceLeases(option82 string, serviceID uint32) (uint32, error) {
	row := s.psql.QueryRow(`SELECT COUNT(ipaddress) FROM dhcp4_leases JOIN dhcp4_subnets ON
	dhcp4_leases.subnet = dhcp4_subnets.id JOIN dhcp4_services ON
	dhcp4_subnets.service = dhcp4_services.id WHERE option82 = $1
	AND dhcp4_services.id = $2`, option82, serviceID)

	var count uint32
	err := row.Scan(&count)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}

		return 0, err
	}

	return count, nil
}

//clientSubnetLeases returns how many leases that are in use for the option82 in given subnet
func (s *Server) clientSubnetLeases(option82 string, subnetID uint32) (uint32, error) {
	row := s.psql.QueryRow(`SELECT COUNT(ipaddress) FROM dhcp4_leases JOIN dhcp4_subnets ON
	dhcp4_leases.subnet = dhcp4_subnets.id WHERE option82 = $1
	AND dhcp4_subnets.id = $2`, option82, subnetID)

	var count uint32
	err := row.Scan(&count)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}

		return 0, err
	}

	return count, nil
}

//clientOptions returns a []byte of the known configured client's options
func (s *Server) clientOptions(client uint32) ([]byte, error) {
	rows, err := s.psql.Query("SELECT option, value FROM dhcp4_clientoptions WHERE client = $1", client)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	//Look for vendor options separately
	vendorOptions, err := s.clientVendorOptions(client)
	if err != nil {
		return nil, err
	}

	var options dhcpOptions
	options.data = append(options.data, vendorOptions...)

	for rows.Next() {
		var option uint8
		var value []byte
		err := rows.Scan(&option, &value)
		if err != nil {
			return nil, err
		}

		//Skip options 43, we've already added them
		if option == 43 {
			continue
		}

		options.addOption(option, value)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return options.data, nil
}

//clientVendorOptions returns a []byte of the vendor options configured for the known client
func (s *Server) clientVendorOptions(client uint32) ([]byte, error) {
	rows, err := s.psql.Query(`SELECT suboption, value, type FROM dhcp4_vendoroptions WHERE name IN
	(SELECT value FROM dhcp4_clientoptions WHERE option = 43 AND client = $1)`, client)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var vendorOptions dhcpOptions
	for rows.Next() {
		var subOption uint8
		var value string
		var vendorType uint8
		err := rows.Scan(&subOption, &value, &vendorType)
		if err != nil {
			return nil, err
		}

		vendorOptions.data = append(vendorOptions.data, subOption)

		switch vendorType {
		case 1: //string
			vendorOptions.data = append(vendorOptions.data, uint8(len(value)))
			vendorOptions.data = append(vendorOptions.data, value...)
		case 2: //uint8
			vendorOptions.data = append(vendorOptions.data, 1)
			vendorOptions.data = append(vendorOptions.data, vendorOptions.intStringtoArray(value, 8)...)
		case 3: //uint16
			vendorOptions.data = append(vendorOptions.data, 2)
			vendorOptions.data = append(vendorOptions.data, vendorOptions.intStringtoArray(value, 16)...)
		case 4: //uint32
			vendorOptions.data = append(vendorOptions.data, 4)
			vendorOptions.data = append(vendorOptions.data, vendorOptions.intStringtoArray(value, 32)...)
		case 5: //IP Address
			vendorOptions.data = append(vendorOptions.data, 4)
			vendorOptions.data = append(vendorOptions.data, net.ParseIP(value).To4()...)
		case 6: //Bool
			vendorOptions.data = append(vendorOptions.data, 1)
			b, _ := strconv.ParseBool(value)
			if b {
				vendorOptions.data = append(vendorOptions.data, 1)
			} else {
				vendorOptions.data = append(vendorOptions.data, 0)
			}
		case 7: //Hex
			vendorOptions.data = append(vendorOptions.data, uint8(len(value)/2))

			h := make([]byte, len(value)/2)
			hex.Decode(h, []byte(value))
			vendorOptions.data = append(vendorOptions.data, h...)
		}
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	totalSize := uint8(len(vendorOptions.data))
	if totalSize == 0 {
		return nil, nil
	}

	/*Insert the main option and total size at the beginning. The value 43 will be at index 0
	and the size at index 1*/
	vendorOptions.data = append([]uint8{totalSize}, vendorOptions.data...)
	vendorOptions.data = append([]uint8{43}, vendorOptions.data...)

	return vendorOptions.data, nil
}

/*addClientOptions compares options from the subnet's pool and replace/add client's options.

The option is at index 0, the size is at index 1 and the value is index 2+.

Returns a copy of poolOptions with the new options*/
func addClientOptions(poolOptions, clientOptions []byte) []byte {
	for c := 0; c < len(clientOptions); {
		clientOption := clientOptions[c]
		clientOptionLength := clientOptions[c+1]
		clientOptionData := clientOptions[c+2 : byte(c)+2+clientOptionLength]
		for p := 0; p < len(poolOptions); {
			poolOptionLength := int(poolOptions[p+1])
			if clientOption == poolOptions[p] {

				//Replace option43 suboptions
				if clientOption == 43 {
					var vendorOptions []byte
					vendorOptions = append(vendorOptions, poolOptions[p+2:p+2+poolOptionLength]...)

					vendorOptions = addClientOptions(vendorOptions, clientOptionData)

					//Replace size
					copy(poolOptions[p+1:p+2], []byte{uint8(len(vendorOptions))})

					//Remove old data
					poolOptions = append(poolOptions[:p+2], poolOptions[p+2+poolOptionLength:]...)

					//Add new data
					poolOptions = append(poolOptions[:p+2], append(vendorOptions, poolOptions[p+2:]...)...)
					break

				} else { //Replace other options

					//Replace size
					copy(poolOptions[p+1:p+2], clientOptions[c+1:c+2])

					//Remove old data
					poolOptions = append(poolOptions[:p+2], poolOptions[p+2+poolOptionLength:]...)

					//Add new data
					poolOptions = append(poolOptions[:p+2], append(clientOptionData, poolOptions[p+2:]...)...)
					break
				}
			}

			p += int(poolOptionLength) + 2

			//If there's no options to replace add the new ones
			if p == len(poolOptions) {
				poolOptions = append(poolOptions, clientOption)
				poolOptions = append(poolOptions, clientOptionLength)
				poolOptions = append(poolOptions, clientOptionData...)
			}
		}

		c += int(clientOptionLength) + 2
	}

	return poolOptions
}

//withinPool returns true if ip within pool
func withinPool(pool []*net.IP, ip net.IP) bool {
	for i := range pool {
		if pool[i].Equal(ip) {
			return true
		}
	}
	return false
}
