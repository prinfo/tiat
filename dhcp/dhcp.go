package dhcp

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/prinfo/tiat/metric"
)

//service defined in dhcp4_services
type service struct {
	ID          uint32 `json:"id"`
	Service     string `json:"service"`
	RelayAgent  string `json:"relayagent"`
	ClientLimit uint16 `json:"clientlimit"`
	subnets     []*subnet
}

//subnet defined in dhcp4_subnets
type subnet struct {
	ID           uint32 `json:"id"`
	Subnet       string `json:"subnet"`
	AllowUnknown bool   `json:"allowunknown"`
	IsStatic     bool   `json:"isstatic"`
	Service      uint32 `json:"service"`
	pool         pool
	options      dhcpOptions
	cidr         *net.IPNet
}

//pool defined in dhcp4_pools
type pool struct {
	ID     uint32 `json:"id"`
	Start  string `json:"poolstart"`
	End    string `json:"poolend"`
	Subnet uint32 `json:"subnet"`
	ips    []*net.IP
}

//poolOption defined in dhcp4_pooloptions
type poolOption struct {
	ID     uint32 `json:"id"`
	Option uint8  `json:"option"`
	Value  string `json:"value"`
	Subnet uint32 `json:"subnet"`
}

//lease defined in dhcp4_leases
type lease struct {
	ID         uint32    `json:"id"`
	IPAddress  string    `json:"ipaddress"`
	MacAddress string    `json:"macaddress"`
	Option82   string    `json:"option82"`
	Subnet     uint32    `json:"subnet"`
	LeaseTime  time.Time `json:"leasetime"`
}

//client defined in dhcp4_knownclients
type client struct {
	ID        uint32      `json:"id"`
	ClientID  string      `json:"clientid"`
	Value     string      `json:"value"`
	IPCount   uint32      `json:"ipcount"`
	Service   uint32      `json:"service"`
	Subnet    interface{} `json:"subnet"` //Can be both NULL and uint32
	Activated time.Time   `json:"activated"`
}

//staticIP defined in dhcp4_staticips
type staticIP struct {
	ID     uint32 `json:"id"`
	IP     string `json:"ipaddress"`
	Client uint32 `json:"client"`
}

//option defined in dhcp4_clientoptions
type clientOption struct {
	ID     uint32 `json:"id"`
	Option uint8  `json:"option"`
	Value  string `json:"value"`
	Client uint32 `json:"client"`
}

//vendorOption defined in dhcp4_vendoroptions
type vendorOption struct {
	ID        uint32 `json:"id"`
	Name      string `json:"name"`
	SubOption uint8  `json:"suboption"`
	Value     string `json:"value"`
	Type      uint32 `json:"type"`
}

//sendError sends an error response to the client
func sendError(w http.ResponseWriter, status int, error string) {
	w.Header().Add("Content-Type", "application/json")
	type errorMsg struct {
		Error string `json:"error"`
	}
	var e errorMsg
	e.Error = error
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(&e)

	go metric.Write("api", map[string]string{"requests": "failed"}, map[string]interface{}{"count": 1}, "")
}

//sendMsg sends a response to the client
func sendMsg(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&data)

	go metric.Write("api", map[string]string{"requests": "successful"}, map[string]interface{}{"count": 1}, "")
}

type deleted struct {
	ID uint32 `json:"deleted_id"`
}

type updated struct {
	ID uint32 `json:"updated_id"`
}

//validToken validates the incoming Authorization header
func (s *Server) validToken(token string) bool {
	for _, t := range s.APITokens {
		if token == t {
			return true
		}
	}
	return false
}

func (s *Server) deleteQuery(w http.ResponseWriter, id string, table string) {
	row := s.psql.QueryRow("DELETE FROM "+table+" WHERE id = $1 RETURNING id", id)

	var rid uint32
	if err := row.Scan(&rid); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, deleted{ID: rid})
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, deleted{ID: rid})
}

//Reload pauses the DHCP process and reloads the configuration
func (s *Server) Reload(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	//We only allow 1 call every 10 seconds
	if !s.limiter.Allow() {
		sendError(w, http.StatusTooEarly, "server busy reloading")
		return
	}

	//If the test fail we don't reload
	if err := Test(s.psql); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		return
	}

	fmt.Println("Reloading DHCP...")
	s.pauseClearTicker <- true
	s.dhcpServices = nil
	if len(s.conns) == 0 {
		for range s.serverAddresses {
			s.conns = append(s.conns, &net.UDPConn{})
		}

	} else {
		for c := range s.conns {
			if s.conns[c] != nil {
				if err := s.conns[c].Close(); err != nil {
					sendError(w, http.StatusInternalServerError, err.Error())
					log.Println(err)
					return
				}
			}
		}
	}

	if err := s.load(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	for c := range s.conns {
		var err error
		s.conns[c], err = net.ListenUDP("udp", &net.UDPAddr{IP: s.serverAddresses[c], Port: 67})
		if err != nil {
			log.Println(err)
			return
		}

		go s.processDhcp(s.conns[c])
	}

	s.startClearLeaseTimer()
	fmt.Println("Reloading Done")
	go metric.Write("api", map[string]string{"requests": "successful"}, map[string]interface{}{"count": 1}, "")
}

//GetServices returns all services from dhcp4_services
func (s *Server) GetServices(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, service, relayagent, clientlimit FROM dhcp4_services")
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	services := []service{}
	for rows.Next() {
		s := service{}
		err := rows.Scan(&s.ID, &s.Service, &s.RelayAgent, &s.ClientLimit)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		services = append(services, s)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, services)
}

//GetService returns a single service from dhcp4_services
func (s *Server) GetService(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	row := s.psql.QueryRow("SELECT id, service, relayagent, clientlimit FROM dhcp4_services WHERE id = $1", mux.Vars(r)["id"])

	service := service{}
	err := row.Scan(&service.ID, &service.Service, &service.RelayAgent, &service.ClientLimit)
	if err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, service)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, service)
}

//AddService adds a single service to dhcp4_services
func (s *Server) AddService(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	service := service{}
	if err := json.NewDecoder(r.Body).Decode(&service); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if service.Service == "" || service.RelayAgent == "" {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("INSERT INTO dhcp4_services (service, relayagent, clientlimit) VALUES  ($1, $2, $3) RETURNING id",
		service.Service, service.RelayAgent, service.ClientLimit)

	if err := row.Scan(&service.ID); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, service)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, service)
}

//DelService deletes a single service from dhcp4_services
func (s *Server) DelService(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	s.deleteQuery(w, mux.Vars(r)["id"], "dhcp4_services")
}

//UpdateService changes a single service on dhcp4_services
func (s *Server) UpdateService(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	service := service{}
	if err := json.NewDecoder(r.Body).Decode(&service); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if service.Service == "" || service.RelayAgent == "" || service.ID == 0 {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("UPDATE dhcp4_services SET service = $1, relayagent = $2, clientlimit = $3 WHERE id = $4 RETURNING id",
		service.Service, service.RelayAgent, service.ClientLimit, service.ID)

	var id uint32
	if err := row.Scan(&id); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, updated{ID: id})
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, updated{ID: id})
}

//GetSubnets returns all subnets from dhcp4_subnets
func (s *Server) GetSubnets(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, subnet, allowunknown, isstatic, service FROM dhcp4_subnets")
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	subnets := []subnet{}
	for rows.Next() {
		s := subnet{}
		err := rows.Scan(&s.ID, &s.Subnet, &s.AllowUnknown, &s.IsStatic, &s.Service)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		subnets = append(subnets, s)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, subnets)
}

//GetSubnet returns a single subnet from dhcp4_subnets
func (s *Server) GetSubnet(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	row := s.psql.QueryRow("SELECT id, subnet, allowunknown, isstatic, service FROM dhcp4_subnets WHERE id = $1", mux.Vars(r)["id"])

	subnet := subnet{}
	err := row.Scan(&subnet.ID, &subnet.Subnet, &subnet.AllowUnknown, &subnet.IsStatic, &subnet.Service)
	if err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, subnet)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, subnet)
}

//AddSubnet adds a single subnet to dhcp4_subnets
func (s *Server) AddSubnet(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	subnet := subnet{}
	if err := json.NewDecoder(r.Body).Decode(&subnet); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if subnet.Subnet == "" || subnet.Service == 0 {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("INSERT INTO dhcp4_subnets (subnet, allowunknown, isstatic, service) VALUES  ($1, $2, $3, $4) RETURNING id",
		subnet.Subnet, subnet.AllowUnknown, subnet.IsStatic, subnet.Service)

	if err := row.Scan(&subnet.ID); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, subnet)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, subnet)
}

//DelSubnet deletes a single subnet from dhcp4_subnets
func (s *Server) DelSubnet(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	s.deleteQuery(w, mux.Vars(r)["id"], "dhcp4_subnets")
}

//UpdateSubnet changes a subnet on dhcp4_subnets
func (s *Server) UpdateSubnet(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	subnet := subnet{}
	if err := json.NewDecoder(r.Body).Decode(&subnet); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if subnet.Subnet == "" || subnet.Service == 0 || subnet.ID == 0 {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("UPDATE dhcp4_subnets SET subnet = $1, allowunknown = $2, isstatic = $3, service = $4 WHERE id = $5 RETURNING id",
		subnet.Subnet, subnet.AllowUnknown, subnet.IsStatic, subnet.Service, subnet.ID)

	var id uint32
	if err := row.Scan(&id); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, updated{ID: id})
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, updated{ID: id})
}

//GetPools returns all pools from dhcp4_pools
func (s *Server) GetPools(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, poolstart, poolend, subnet FROM dhcp4_pools")
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	pools := []pool{}
	for rows.Next() {
		p := pool{}
		err := rows.Scan(&p.ID, &p.Start, &p.End, &p.Subnet)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		pools = append(pools, p)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, pools)
}

//GetPool returns a single pool from dhcp4_pools
func (s *Server) GetPool(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	row := s.psql.QueryRow("SELECT id, poolstart, poolend, subnet FROM dhcp4_pools WHERE id = $1", mux.Vars(r)["id"])

	pool := pool{}
	err := row.Scan(&pool.ID, &pool.Start, &pool.End, &pool.Subnet)
	if err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, pool)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, pool)
}

//AddPool adds a single pool to dhcp4_pools
func (s *Server) AddPool(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	pool := pool{}
	if err := json.NewDecoder(r.Body).Decode(&pool); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if pool.Start == "" || pool.End == "" || pool.Subnet == 0 {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("INSERT INTO dhcp4_pools (poolstart, poolend, subnet) VALUES  ($1, $2, $3) RETURNING id",
		pool.Start, pool.End, pool.Subnet)

	if err := row.Scan(&pool.ID); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, pool)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, pool)
}

//DelPool deletes a single pool from dhcp4_pools
func (s *Server) DelPool(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	s.deleteQuery(w, mux.Vars(r)["id"], "dhcp4_pools")
}

//UpdatePool changes a pool on dhcp4_pools
func (s *Server) UpdatePool(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	pool := pool{}
	if err := json.NewDecoder(r.Body).Decode(&pool); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if pool.ID == 0 || pool.Start == "" || pool.End == "" {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("UPDATE dhcp4_pools SET poolstart = $1, poolend = $2 WHERE id = $3 RETURNING id",
		pool.Start, pool.End, pool.ID)

	var id uint32
	if err := row.Scan(&id); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, updated{ID: id})
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, updated{ID: id})
}

//GetPoolsOptions returns all pools options from dhcp4_pooloptions
func (s *Server) GetPoolsOptions(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, option, value, subnet FROM dhcp4_pooloptions")
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	poolOpts := []poolOption{}
	for rows.Next() {
		p := poolOption{}
		err := rows.Scan(&p.ID, &p.Option, &p.Value, &p.Subnet)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		poolOpts = append(poolOpts, p)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, poolOpts)
}

//GetPoolOptions returns all pool's options from dhcp4_pooloptions
func (s *Server) GetPoolOptions(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, options, value, subnet FROM dhcp4_pooloptions WHERE subnet = $1", mux.Vars(r)["subnet"])
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	options := []poolOption{}
	for rows.Next() {
		o := poolOption{}
		err := rows.Scan(&o.ID, &o.Option, &o.Value, &o.Subnet)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		options = append(options, o)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, options)
}

//AddPoolOption adds a single option to dhcp4_pooloptions
func (s *Server) AddPoolOption(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	option := poolOption{}
	if err := json.NewDecoder(r.Body).Decode(&option); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if option.Option == 0 || option.Value == "" || option.Subnet == 0 {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("INSERT INTO dhcp4_pooloptions (option, value, subnet) VALUES  ($1, $2, $3) RETURNING id",
		option.Option, option.Value, option.Subnet)

	if err := row.Scan(&option.ID); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, option)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, option)
}

//DelPoolOption deletes a single option from dhcp4_pooloptions
func (s *Server) DelPoolOption(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	s.deleteQuery(w, mux.Vars(r)["id"], "dhcp4_pooloptions")
}

//UpdatePoolOption changes a single option on dhcp4_pooloptions
func (s *Server) UpdatePoolOption(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	option := poolOption{}
	if err := json.NewDecoder(r.Body).Decode(&option); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if option.ID == 0 || option.Value == "" {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("UPDATE dhcp4_pooloptions SET value = $1 WHERE id = $2 RETURNING id", option.Value, option.ID)

	var id uint32
	if err := row.Scan(&id); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, updated{ID: id})
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, updated{ID: id})
}

//GetLeases returns all leases from dhcp4_leases
func (s *Server) GetLeases(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, ipaddress, macaddress, option82, subnet, leasetime FROM dhcp4_leases")
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	leases := []lease{}
	for rows.Next() {
		l := lease{}
		err = rows.Scan(&l.ID, &l.IPAddress, &l.MacAddress, &l.Option82, &l.Subnet, &l.LeaseTime)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		leases = append(leases, l)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, leases)
}

//GetLeaseByMac returns a single lease from dhcp4_leases based on given mac address
func (s *Server) GetLeaseByMac(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}
	mac := mux.Vars(r)["mac"]

	if _, err := net.ParseMAC(mac); err != nil {
		sendError(w, http.StatusBadRequest, "invalid mac address")
		return
	}

	row := s.psql.QueryRow("SELECT id, ipaddress, macaddress, option82, subnet, leasetime FROM dhcp4_leases WHERE macaddress = $1",
		mac)

	l := lease{}
	if err := row.Scan(&l.ID, &l.IPAddress, &l.MacAddress, &l.Option82, &l.Subnet, &l.LeaseTime); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, l)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, l)
}

//GetLeaseByOpt82 returns a single lease from dhcp4_leases based on given option82
func (s *Server) GetLeaseByOpt82(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}
	opt82 := mux.Vars(r)["opt82"]

	rows, err := s.psql.Query("SELECT id, ipaddress, macaddress, option82, subnet, leasetime FROM dhcp4_leases WHERE option82 = $1",
		opt82)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	leases := []lease{}
	for rows.Next() {
		l := lease{}
		err := rows.Scan(&l.ID, &l.IPAddress, &l.MacAddress, &l.Option82, &l.Subnet, &l.LeaseTime)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		leases = append(leases, l)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, leases)
}

//DelLease deletes a single lease from dhcp4_leases based on given mac and ip address
func (s *Server) DelLease(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	mac, err := net.ParseMAC(mux.Vars(r)["mac"])
	if err != nil {
		sendError(w, http.StatusBadRequest, "invalid mac address")
		return
	}

	ip := r.URL.Query().Get("ip")
	if ip == "" {
		sendError(w, http.StatusBadRequest, "invalid ip address")
		return
	}

	if err := s.removeLease(net.ParseIP(ip), mac); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
}

//GetClients returns all clients from dhcp4_knownclients
func (s *Server) GetClients(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, clientid, value, ipcount, service, subnet, activated FROM dhcp4_knownclients")
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	clients := []client{}
	for rows.Next() {
		c := client{}
		err := rows.Scan(&c.ID, &c.ClientID, &c.Value, &c.IPCount, &c.Service, &c.Subnet, &c.Activated)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		clients = append(clients, c)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, clients)
}

//GetClient returns a client entry from dhcp4_knownclients based on it's id
func (s *Server) GetClient(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	row := s.psql.QueryRow("SELECT id, clientid, value, ipcount, service, subnet, activated FROM dhcp4_knownclients WHERE id = $1", mux.Vars(r)["id"])

	client := client{}
	err := row.Scan(&client.ID, &client.ClientID, &client.Value, &client.IPCount, &client.Service, &client.Subnet, &client.Activated)
	if err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, client)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, client)
}

//GetClientByValue returns a client entry from dhcp4_knownclients based on it's value
func (s *Server) GetClientByValue(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	row := s.psql.QueryRow("SELECT id, clientid, value, ipcount, service, subnet, activated FROM dhcp4_knownclients WHERE value = $1", mux.Vars(r)["value"])

	client := client{}
	err := row.Scan(&client.ID, &client.ClientID, &client.Value, &client.IPCount, &client.Service, &client.Subnet, &client.Activated)
	if err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, client)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, client)
}

//GetClientByClientID returns all entries for a clientid from dhcp4_knownclients
func (s *Server) GetClientByClientID(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, clientid, value, ipcount, service, subnet, activated FROM dhcp4_knownclients WHERE clientid = $1", mux.Vars(r)["clientid"])
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	clients := []client{}
	for rows.Next() {
		c := client{}
		err := rows.Scan(&c.ID, &c.ClientID, &c.Value, &c.IPCount, &c.Service, &c.Subnet, &c.Activated)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		clients = append(clients, c)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, clients)
}

//AddClient adds a single client to dhcp4_knownclients
func (s *Server) AddClient(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	client := client{}
	if err := json.NewDecoder(r.Body).Decode(&client); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if client.Service == 0 || client.ClientID == "" || client.Value == "" {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	if client.IPCount == 0 {
		client.IPCount = 1
	}

	row := s.psql.QueryRow("INSERT INTO dhcp4_knownclients (clientid, value, ipcount, service, subnet) VALUES  ($1, $2, $3, $4, $5) RETURNING id",
		client.ClientID, client.Value, client.IPCount, client.Service, client.Subnet)

	if err := row.Scan(&client.ID); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, client)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, client)
}

//DelClient deletes a single client from dhcp4_knownclients
func (s *Server) DelClient(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	s.deleteQuery(w, mux.Vars(r)["id"], "dhcp4_knownclients")
}

//UpdateClient changes a single client on dhcp4_knownclients
func (s *Server) UpdateClient(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	client := client{}
	if err := json.NewDecoder(r.Body).Decode(&client); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if client.Service == 0 || client.ClientID == "" || client.Value == "" ||
		client.ID == 0 || client.IPCount == 0 {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("UPDATE dhcp4_knownclients SET clientid = $1, value = $2, ipcount = $3, service = $4, subnet = $5 WHERE id = $6 RETURNING id",
		client.ClientID, client.Value, client.IPCount, client.Service, client.Subnet, client.ID)

	var id uint32
	if err := row.Scan(&id); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, updated{ID: id})
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, updated{ID: id})
}

//GetIPs returns all static IPs from dhcp4_staticips
func (s *Server) GetIPs(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, ipaddress, client FROM dhcp4_staticips")
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	staticIPs := []staticIP{}
	for rows.Next() {
		s := staticIP{}
		err := rows.Scan(&s.ID, &s.IP, &s.Client)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		staticIPs = append(staticIPs, s)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, staticIPs)
}

//GetIP returns the configured IPs for the client from dhcp4_staticips
func (s *Server) GetIP(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, ipaddress, client FROM dhcp4_staticips WHERE client = $1", mux.Vars(r)["clientid"])
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	staticIPs := []staticIP{}
	for rows.Next() {
		s := staticIP{}
		err := rows.Scan(&s.ID, &s.IP, &s.Client)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		staticIPs = append(staticIPs, s)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, staticIPs)
}

//AddIP adds a single ip to dhcp4_staticips
func (s *Server) AddIP(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	staticIP := staticIP{}
	if err := json.NewDecoder(r.Body).Decode(&staticIP); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if staticIP.IP == "" || staticIP.Client == 0 {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("INSERT INTO dhcp4_staticips (ipaddress, client) VALUES  ($1, $2) RETURNING id",
		staticIP.IP, staticIP.Client)

	if err := row.Scan(&staticIP.ID); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, staticIP)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, staticIP)
}

//DelIP deletes a single ip from dhcp4_staticips
func (s *Server) DelIP(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	s.deleteQuery(w, mux.Vars(r)["id"], "dhcp4_staticips")
}

//UpdateIP changes a single ip on dhcp4_staticips
func (s *Server) UpdateIP(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	staticIP := staticIP{}
	if err := json.NewDecoder(r.Body).Decode(&staticIP); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if staticIP.IP == "" || staticIP.ID == 0 {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("UPDATE dhcp4_staticips SET ipaddress = $1 WHERE id = $2 RETURNING id", staticIP.IP, staticIP.ID)

	var id uint32
	if err := row.Scan(&id); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, updated{ID: id})
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, updated{ID: id})
}

//GetClientsOptions returns all clients options from dhcp4_clientoptions
func (s *Server) GetClientsOptions(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, option, value, client FROM dhcp4_clientoptions")
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	options := []clientOption{}
	for rows.Next() {
		o := clientOption{}
		err := rows.Scan(&o.ID, &o.Option, &o.Value, &o.Client)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		options = append(options, o)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, options)
}

//GetClientOptions returns all client's options from dhcp4_clientoptions
func (s *Server) GetClientOptions(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, option, value, client FROM dhcp4_clientoptions WHERE client = $1", mux.Vars(r)["clientid"])
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	options := []clientOption{}
	for rows.Next() {
		o := clientOption{}
		err := rows.Scan(&o.ID, &o.Option, &o.Value, &o.Client)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		options = append(options, o)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, options)
}

//AddClientOption adds a single option to dhcp4_clientoptions
func (s *Server) AddClientOption(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	option := clientOption{}
	if err := json.NewDecoder(r.Body).Decode(&option); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if option.Option == 0 || option.Value == "" || option.Client == 0 {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("INSERT INTO dhcp4_clientoptions (option, value, client) VALUES  ($1, $2, $3) RETURNING id",
		option.Option, option.Value, option.Client)

	if err := row.Scan(&option.ID); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, option)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, option)
}

//DelClientOption deletes a single option from dhcp4_clientoptions
func (s *Server) DelClientOption(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	s.deleteQuery(w, mux.Vars(r)["id"], "dhcp4_clientoptions")
}

//UpdateClientOption changes a single option on dhcp4_clientoptions
func (s *Server) UpdateClientOption(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	option := clientOption{}
	if err := json.NewDecoder(r.Body).Decode(&option); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if option.ID == 0 || option.Value == "" {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("UPDATE dhcp4_clientoptions SET value = $1 WHERE id = $2 RETURNING id", option.Value, option.ID)

	var id uint32
	if err := row.Scan(&id); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, updated{ID: id})
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, updated{ID: id})
}

//GetVendorOptions returns all options from dhcp4_vendoroptions
func (s *Server) GetVendorOptions(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	rows, err := s.psql.Query("SELECT id, name, suboption, value, type FROM dhcp4_vendoroptions")
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}
	defer rows.Close()

	options := []vendorOption{}
	for rows.Next() {
		o := vendorOption{}
		err := rows.Scan(&o.ID, &o.Name, &o.SubOption, &o.Value, &o.Type)
		if err != nil {
			sendError(w, http.StatusInternalServerError, err.Error())
			log.Println(err.Error())
			return
		}

		options = append(options, o)
	}

	if err := rows.Err(); err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, options)
}

//GetVendorOption returns a single option from dhcp4_vendoroptions
func (s *Server) GetVendorOption(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	row := s.psql.QueryRow("SELECT id, name, suboption, value, type FROM dhcp4_vendoroptions WHERE id = $1", mux.Vars(r)["id"])

	option := vendorOption{}
	err := row.Scan(&option.ID, &option.Name, &option.SubOption, &option.Value, &option.Type)
	if err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, option)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, option)
}

//AddVendorOption adds a single option to dhcp4_vendoroptions
func (s *Server) AddVendorOption(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	option := vendorOption{}
	if err := json.NewDecoder(r.Body).Decode(&option); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if option.Name == "" || option.SubOption == 0 || option.Value == "" || option.Type == 0 {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("INSERT INTO dhcp4_vendoroptions (name, suboption, value, type) VALUES  ($1, $2, $3, $4) RETURNING id",
		option.Name, option.SubOption, option.Value, option.Type)

	if err := row.Scan(&option.ID); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, option)
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, option)
}

//DelVendorOption deletes a single option from dhcp4_vendoroptions
func (s *Server) DelVendorOption(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	s.deleteQuery(w, mux.Vars(r)["id"], "dhcp4_vendoroptions")
}

//UpdateVendorOption changes a single option on dhcp4_vendoroptions
func (s *Server) UpdateVendorOption(w http.ResponseWriter, r *http.Request) {
	if !s.validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	option := vendorOption{}
	if err := json.NewDecoder(r.Body).Decode(&option); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if option.Name == "" || option.ID == 0 || option.Value == "" || option.Type == 0 {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	row := s.psql.QueryRow("UPDATE dhcp4_vendoroptions SET name = $1, value = $2, type = $3 WHERE id = $4 RETURNING id",
		option.Name, option.Value, option.Type, option.ID)

	var id uint32
	if err := row.Scan(&id); err != nil {
		if err == sql.ErrNoRows {
			sendMsg(w, updated{ID: id})
			return
		}

		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err.Error())
		return
	}

	sendMsg(w, updated{ID: id})
}
