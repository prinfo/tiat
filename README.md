Tiat
----

Tiat is an application made for service provisioning.

Written in Go.

Tiat DHCP
---------

Tiat DHCP is a DHCPv4 server that uses PostgreSQL as backend.

When the program starts it will query the database to retrieve configured networks, subnets, pools and options and load it in memory.
It also check for existing leases to add or remove from the pools.

When the program receives a packet it will try match the giaddr(Relay Agent IP) with the configured services.
If there's a match it will query the database for known clients and existing leases and update the database accordingly.
You can configure a subnet to allow unknown clients. A service can have a client limit for subnets that allows unknown clients.

Every 5 seconds the process will query the database for leases and remove those who are older than twice its subnet lease time.

You can add and remove clients, client options and IP reservations from the database while the program is running, but if you edit services, subnets, pools or options the process needs to reload.

If you want to remove a lease while the server is running you can use the API (See API.md). Do not add or remove a lease manually without restarting the server.

Tiat Metrics
------------

When enabled Tiat will send statistics on leases, DHCP messages and response time to InfluxDB.

Tiat BECS
---------

Tiat can be enabled to be a middleware between an external application and BECS.

It has functions to retrieve status of an ASR and its interfaces, show clients and handle services.
