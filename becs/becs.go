package becs

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	becs "gitlab.com/prinfo/becsclient"
	"gitlab.com/prinfo/tiat/metric"
)

var (
	//Host url to BECS
	Host string

	//Username to BECS
	Username string

	//Password to BECS
	Password string

	//APITokens for authorization
	APITokens []string
)

//sendError sends an error response to the client
func sendError(w http.ResponseWriter, status int, error string) {
	w.Header().Add("Content-Type", "application/json")
	type errorMsg struct {
		Error string `json:"error"`
	}
	var e errorMsg
	e.Error = error
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(&e)

	go metric.Write("api", map[string]string{"requests": "failed"}, map[string]interface{}{"count": 1}, "")
}

//sendMsg sends a response to the client
func sendMsg(w http.ResponseWriter, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(&data); err != nil {
		log.Println("JSON Encode:", err)
	}

	go metric.Write("api", map[string]string{"requests": "successful"}, map[string]interface{}{"count": 1}, "")
}

//validToken validates the incoming Authorization header
func validToken(token string) bool {
	for _, t := range APITokens {
		if token == t {
			return true
		}
	}
	return false
}

//GetASRs returns all ibos ASRs
func GetASRs(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	search := becs.Object{}
	search.Parameters = &struct {
		Items []becs.ParameterKey "xml:\"item\""
	}{}

	search.Class = "element-attach"

	value := becs.ParameterValue{
		Value: "asr*",
	}

	key := becs.ParameterKey{
		Name: "platform",
	}

	key.Values.Items = append(key.Values.Items, value)
	search.Parameters.Items = append(search.Parameters.Items, key)

	asrs, err := becs.ObjectFind(search)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		return
	}

	if len(asrs) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	sendMsg(w, asrs)
}

//GetASR returns a single ibos ASRs by name
func GetASR(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	search := becs.Object{
		Name:  mux.Vars(r)["name"],
		Class: "element-attach",
	}

	asrs, err := becs.ObjectFind(search)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	if len(asrs) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	sendMsg(w, asrs)
}

//GetASRStatus returns the status of an ASR
func GetASRStatus(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	cell := mux.Vars(r)["cell"]
	oid, _ := strconv.ParseUint(mux.Vars(r)["oid"], 10, 0)
	status, err := becs.ElementStatusGet(cell, []uint64{oid})
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	if len(status) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	sendMsg(w, status)
}

//GetASRInterfaces returns all interfaces from the given ASR OID
func GetASRInterfaces(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	oid, _ := strconv.ParseUint(mux.Vars(r)["asroid"], 10, 0)
	interfaces, err := becs.ObjectTreeFind(oid, "interface", 1)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		return
	}

	if len(interfaces) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	sendMsg(w, interfaces)
}

//GetInterfaceStatus returns the status of an interface
func GetInterfaceStatus(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	status, _, err := becs.InterfaceInfoGet(becs.TypeNameValue{
		Type:  "oid",
		Value: mux.Vars(r)["oid"],
	}, 0, []string{"sfp", "status", "media"}, "", nil)
	if err != nil {
		log.Println(err)
		return
	}

	if len(status) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	sendMsg(w, status)
}

//GetServices gathers all services from given object oid
func GetServices(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	oid, _ := strconv.ParseUint(mux.Vars(r)["parentoid"], 10, 0)
	services, err := becs.ServiceTreeFind(oid, r.URL.Query().Get("servicename"), r.URL.Query().Get("namespace"))
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	if len(services) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	sendMsg(w, services)
}

//AddService creates a service under given parent oid. Returns the saoid for the newly created service
func AddService(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	type input struct {
		InterfaceOID uint64                     `xml:"interfaceoid"`
		Service      string                     `xml:"service"`
		Namespace    string                     `xml:"namespace"`
		Attributes   []becs.ServiceMapAttribute `xml:"attributes"`
	}

	service := input{}

	if err := json.NewDecoder(r.Body).Decode(&service); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if service.InterfaceOID == 0 || service.Service == "" {
		sendError(w, http.StatusBadRequest, "missing required fields")
		return
	}

	saoid, err := becs.ServiceMapCreate(service.InterfaceOID, service.Service, service.Namespace, service.Attributes...)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	if saoid == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	sendMsg(w, saoid)
}

//DelService removes a service
func DelService(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	saoid, _ := strconv.ParseUint(mux.Vars(r)["saoid"], 10, 0)
	err := becs.ServiceMapDelete(saoid)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}
}

//DisableService disables a service
func DisableService(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	saoid, _ := strconv.ParseUint(mux.Vars(r)["saoid"], 10, 0)
	err := becs.ServiceMapDisable(saoid)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}
}

//EnableService enables a service
func EnableService(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	saoid, _ := strconv.ParseUint(mux.Vars(r)["saoid"], 10, 0)
	err := becs.ServiceMapEnable(saoid)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}
}

//ModifyService modifies a service. Returns the saoid for modified service
func ModifyService(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	type input struct {
		Saoid      uint64                     `xml:"saoid"`
		Attributes []becs.ServiceMapAttribute `xml:"attributes"`
	}

	service := input{}

	if err := json.NewDecoder(r.Body).Decode(&service); err != nil {
		sendError(w, http.StatusBadRequest, err.Error())
		return
	}

	if service.Saoid == 0 {
		sendError(w, http.StatusBadRequest, "saoid required")
		return
	}

	saoid, err := becs.ServiceMapModify(service.Saoid, service.Attributes...)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		log.Println(err)
		return
	}

	if saoid == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	sendMsg(w, saoid)
}

//GetClients returns clients from an interface
func GetClients(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	oid, _ := strconv.ParseUint(mux.Vars(r)["interfaceoid"], 10, 0)
	clients, _, err := becs.ClientFind("", oid, 0)
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		return
	}

	if len(clients) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	sendMsg(w, clients)
}

//DelClient removes a client from an ASR
func DelClient(w http.ResponseWriter, r *http.Request) {
	if !validToken(r.Header.Get("Authorization")) {
		sendError(w, http.StatusUnauthorized, "unauthorized")
		return
	}

	if err := becs.SessionPing(); err != nil {
		if err := becs.SessionLogin(Host, Username, Password); err != nil {
			sendError(w, http.StatusUnauthorized, err.Error())
			return
		}
	}

	oid, _ := strconv.ParseUint(mux.Vars(r)["asroid"], 10, 0)
	err := becs.ContextReset(oid, r.URL.Query().Get("context"), r.URL.Query().Get("reason"))
	if err != nil {
		sendError(w, http.StatusInternalServerError, err.Error())
		return
	}
}
