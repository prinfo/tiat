package metric

import (
	"time"

	influx "github.com/influxdata/influxdb-client-go"
	"github.com/influxdata/influxdb-client-go/api"
)

var (
	//Enabled if open() is called
	Enabled bool

	tiatAddress  string
	influxWrite  api.WriteApi
	influxClient influx.Client
)

//Open creates a new InfluxDB client
func Open(url, database string) {
	options := influx.Options{}
	options.SetMaxRetries(0)
	options.SetHttpRequestTimeout(2)
	influxClient = influx.NewClientWithOptions(url, "", &options)
	influxWrite = influxClient.WriteApi("", database)
	Enabled = true
}

//Close removes the InfluxDB client
func Close() {
	influxClient.Close()
}

//Write sends a new point to InfluxDB
func Write(measurement string, tags map[string]string, fields map[string]interface{}, server string) {
	if Enabled {
		p := influx.NewPoint(measurement, tags, fields, time.Now())
		if server == "" {
			server = tiatAddress
		}
		p.AddTag("server", server)
		influxWrite.WritePoint(p)
	}
}
