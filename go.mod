module gitlab.com/prinfo/tiat

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/influxdata/influxdb-client-go v1.4.0
	github.com/lib/pq v1.10.0
	gitlab.com/prinfo/becsclient v0.0.11
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba
)
